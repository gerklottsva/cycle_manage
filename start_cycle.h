#ifndef START_CYCLE_H
#define START_CYCLE_H

#include <QWidget>
#include "ui_start_cycle.h"
#include <QDesktopServices>
#include <QUrl>

namespace Ui {
class start_cycle;
}

class start_cycle : public QWidget
{
    Q_OBJECT

public:
    explicit start_cycle(QWidget *parent = nullptr);
    ~start_cycle();

public slots:
    void start_file();
    void stop_file();

private:
    Ui::start_cycle *ui;
};

#endif // START_CYCLE_H
