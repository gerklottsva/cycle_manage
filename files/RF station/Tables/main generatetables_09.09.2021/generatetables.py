#Генерация файлов таблиц для модуля-Тестера и модуля-контроллера бустера НИКА
#Фатькин Г. А. 27.11.2014, Дубна

#Имена файлов для записи таблиц
HV1Stage1_filename = "controller_HV1Stage1.txt"
HV2Stage1_filename = "controller_HV2Stage1.txt"
HV1Stage2_filename = "controller_HV1Stage2.txt"
HV2Stage2_filename = "controller_HV2Stage2.txt"

#Массив напряжений ВЧ для станции 1 в первом этапе (время отсчитывается от Bref)
U_HV1Stage1_points = [
[1,2,3,1,5,6,7,1,1,1,1,1,1,1,1],	#Время, с
[0,52,1,1,1,1,1,1,1,1,1,1,1,1,1]]	#U, kV
#Массив напряжений ВЧ для станции 2 в первом этапе (время отсчитывается от Bref)
U_HV2Stage1_points = U_HV1Stage1_points #Сейчас напряжения ВЧ - одинаковые

#Массив напряжений ВЧ для станции 1 во втором этапе (время отсчитывается от Старт II)
U_HV1Stage2_points = [
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],	#Время, с
[0,1,1,1,1,1,1,1,5,1,1,1,1,1,1]]	#U, kV
#Массив напряжений ВЧ для станции 2 во втором этапе (время отсчитывается от Старт II)
U_HV2Stage2_points = U_HV1Stage2_points

deltaT_tester = 5e-6 # Отсчёт тестера: 5 мкс
deltaT_controller = 20e-6 # Отсчёт контроллера: 20 мкс

#------------------------------------------------------------------------------------
# Конец конфигурации
#------------------------------------------------------------------------------------


import matplotlib.pyplot as plt
from math import ceil
import numpy as np


#Линейная экстраполяция по точкам points с шагом step
#На входе массив неэкстраполированных точек [[x1,x2,...],[y1,y2,...]], отсортированный по x,
#не должен содержать точек с одинаковыми x.
#Возвращает массив экстраполированных точек [[x1,x1+step,...][y1,y1',....]]
def interpolate_linear(points,step):
    result = [[],[]]
    for i,point in enumerate(points[0][0:-1]):
        for x in np.arange(points[0][i],points[0][i+1],step):
            result[0].append(x)
            result[1].append(points[1][i] + (points[1][i+1]-points[1][i])/(points[0][i+1]-points[0][i])*(x-points[0][i]))
    result[0].append(points[0][-1])
    result[1].append(points[1][-1])
    return result

#Генерация таблиц:
U_HV1Stage1 = interpolate_linear(U_HV1Stage1_points,deltaT_controller)
U_HV2Stage1 = interpolate_linear(U_HV2Stage1_points,deltaT_controller)
U_HV1Stage2 = interpolate_linear(U_HV1Stage2_points,deltaT_controller)
U_HV2Stage2 = interpolate_linear(U_HV2Stage2_points,deltaT_controller)


# Отрисовка таблиц на отдельных графиках
def drawTables():
    plt.figure(1)
    plt.subplot(311)
    plt.plot(U_tester[0],U_tester[1])
    plt.ylabel('U_tester [V]')
    plt.grid(True)
    plt.subplot(312)
    plt.plot(U_HV1Stage1[0],U_HV1Stage1[1])
    plt.plot(U_HV2Stage1[0],U_HV2Stage1[1])
    plt.ylabel('U_HV Stage1 [kV]')
    plt.legend(['Station1','Station2'])
    plt.grid(True)
    plt.subplot(313)
    plt.plot(U_HV1Stage2[0],U_HV1Stage2[1])
    plt.plot(U_HV2Stage2[0],U_HV2Stage2[1])
    plt.ylabel('U_HV Stage2 [kV]')
    plt.legend(['Station1','Station2'])
    plt.grid(True)
    plt.show()    

def write_Controller(filename,table):
    with open(filename,"w") as f:
        for u in table:
            f.write("{0:.6}\n".format(u))

if __name__ == '__main__':
    print("Генерируем файлы для контроллера")
    write_Controller(HV1Stage1_filename,U_HV1Stage1[1])
    write_Controller(HV2Stage1_filename,U_HV2Stage1[1])
    write_Controller(HV1Stage2_filename,U_HV1Stage2[1])
    write_Controller(HV2Stage2_filename,U_HV2Stage2[1])
    print("Готово!")
    drawTables()