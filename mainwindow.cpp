#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("Управление циклом");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_startStopCycleButton_clicked()
{
    start_cycle.show();
}

void MainWindow::on_coefficientsButton_clicked()
{
    coefficients.show();
}

void MainWindow::on_timeVoltageButton_clicked()
{
    time_voltage.show();
}

void MainWindow::on_b_kButton_clicked()
{
    b_k.show();
}

void MainWindow::on_graphButton_clicked()
{
    graph.show();
}
