/****************************************************************************
** Meta object code from reading C++ file 'b_k.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../b_k.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'b_k.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_b_k_t {
    QByteArrayData data[20];
    char stringdata0[312];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_b_k_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_b_k_t qt_meta_stringdata_b_k = {
    {
QT_MOC_LITERAL(0, 0, 3), // "b_k"
QT_MOC_LITERAL(1, 4, 19), // "ui_write_to_file_B0"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 18), // "ui_write_to_file_K"
QT_MOC_LITERAL(4, 44, 13), // "plus_value_B0"
QT_MOC_LITERAL(5, 58, 14), // "minus_value_B0"
QT_MOC_LITERAL(6, 73, 16), // "oneplus_value_B0"
QT_MOC_LITERAL(7, 90, 17), // "oneminus_value_B0"
QT_MOC_LITERAL(8, 108, 16), // "twoplus_value_B0"
QT_MOC_LITERAL(9, 125, 17), // "twominus_value_B0"
QT_MOC_LITERAL(10, 143, 18), // "threeplus_value_B0"
QT_MOC_LITERAL(11, 162, 19), // "threeminus_value_B0"
QT_MOC_LITERAL(12, 182, 12), // "plus_value_K"
QT_MOC_LITERAL(13, 195, 13), // "minus_value_K"
QT_MOC_LITERAL(14, 209, 15), // "oneplus_value_K"
QT_MOC_LITERAL(15, 225, 16), // "oneminus_value_K"
QT_MOC_LITERAL(16, 242, 15), // "twoplus_value_K"
QT_MOC_LITERAL(17, 258, 16), // "twominus_value_K"
QT_MOC_LITERAL(18, 275, 17), // "threeplus_value_K"
QT_MOC_LITERAL(19, 293, 18) // "threeminus_value_K"

    },
    "b_k\0ui_write_to_file_B0\0\0ui_write_to_file_K\0"
    "plus_value_B0\0minus_value_B0\0"
    "oneplus_value_B0\0oneminus_value_B0\0"
    "twoplus_value_B0\0twominus_value_B0\0"
    "threeplus_value_B0\0threeminus_value_B0\0"
    "plus_value_K\0minus_value_K\0oneplus_value_K\0"
    "oneminus_value_K\0twoplus_value_K\0"
    "twominus_value_K\0threeplus_value_K\0"
    "threeminus_value_K"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_b_k[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x0a /* Public */,
       3,    0,  105,    2, 0x0a /* Public */,
       4,    0,  106,    2, 0x0a /* Public */,
       5,    0,  107,    2, 0x0a /* Public */,
       6,    0,  108,    2, 0x0a /* Public */,
       7,    0,  109,    2, 0x0a /* Public */,
       8,    0,  110,    2, 0x0a /* Public */,
       9,    0,  111,    2, 0x0a /* Public */,
      10,    0,  112,    2, 0x0a /* Public */,
      11,    0,  113,    2, 0x0a /* Public */,
      12,    0,  114,    2, 0x0a /* Public */,
      13,    0,  115,    2, 0x0a /* Public */,
      14,    0,  116,    2, 0x0a /* Public */,
      15,    0,  117,    2, 0x0a /* Public */,
      16,    0,  118,    2, 0x0a /* Public */,
      17,    0,  119,    2, 0x0a /* Public */,
      18,    0,  120,    2, 0x0a /* Public */,
      19,    0,  121,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void b_k::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<b_k *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ui_write_to_file_B0(); break;
        case 1: _t->ui_write_to_file_K(); break;
        case 2: _t->plus_value_B0(); break;
        case 3: _t->minus_value_B0(); break;
        case 4: _t->oneplus_value_B0(); break;
        case 5: _t->oneminus_value_B0(); break;
        case 6: _t->twoplus_value_B0(); break;
        case 7: _t->twominus_value_B0(); break;
        case 8: _t->threeplus_value_B0(); break;
        case 9: _t->threeminus_value_B0(); break;
        case 10: _t->plus_value_K(); break;
        case 11: _t->minus_value_K(); break;
        case 12: _t->oneplus_value_K(); break;
        case 13: _t->oneminus_value_K(); break;
        case 14: _t->twoplus_value_K(); break;
        case 15: _t->twominus_value_K(); break;
        case 16: _t->threeplus_value_K(); break;
        case 17: _t->threeminus_value_K(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject b_k::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_b_k.data,
    qt_meta_data_b_k,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *b_k::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *b_k::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_b_k.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int b_k::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
