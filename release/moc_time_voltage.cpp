/****************************************************************************
** Meta object code from reading C++ file 'time_voltage.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../time_voltage.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'time_voltage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_time_voltage_t {
    QByteArrayData data[123];
    char stringdata0[2787];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_time_voltage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_time_voltage_t qt_meta_stringdata_time_voltage = {
    {
QT_MOC_LITERAL(0, 0, 12), // "time_voltage"
QT_MOC_LITERAL(1, 13, 22), // "ui_read_from_file_t1_1"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 22), // "ui_read_from_file_t2_1"
QT_MOC_LITERAL(4, 60, 22), // "ui_read_from_file_t3_1"
QT_MOC_LITERAL(5, 83, 22), // "ui_read_from_file_t4_1"
QT_MOC_LITERAL(6, 106, 22), // "ui_read_from_file_t5_1"
QT_MOC_LITERAL(7, 129, 22), // "ui_read_from_file_t6_1"
QT_MOC_LITERAL(8, 152, 22), // "ui_read_from_file_t7_1"
QT_MOC_LITERAL(9, 175, 22), // "ui_read_from_file_t8_1"
QT_MOC_LITERAL(10, 198, 22), // "ui_read_from_file_t9_1"
QT_MOC_LITERAL(11, 221, 23), // "ui_read_from_file_t10_1"
QT_MOC_LITERAL(12, 245, 23), // "ui_read_from_file_t11_1"
QT_MOC_LITERAL(13, 269, 23), // "ui_read_from_file_t12_1"
QT_MOC_LITERAL(14, 293, 23), // "ui_read_from_file_t13_1"
QT_MOC_LITERAL(15, 317, 23), // "ui_read_from_file_t14_1"
QT_MOC_LITERAL(16, 341, 23), // "ui_read_from_file_t15_1"
QT_MOC_LITERAL(17, 365, 22), // "ui_read_from_file_u1_1"
QT_MOC_LITERAL(18, 388, 22), // "ui_read_from_file_u2_1"
QT_MOC_LITERAL(19, 411, 22), // "ui_read_from_file_u3_1"
QT_MOC_LITERAL(20, 434, 22), // "ui_read_from_file_u4_1"
QT_MOC_LITERAL(21, 457, 22), // "ui_read_from_file_u5_1"
QT_MOC_LITERAL(22, 480, 22), // "ui_read_from_file_u6_1"
QT_MOC_LITERAL(23, 503, 22), // "ui_read_from_file_u7_1"
QT_MOC_LITERAL(24, 526, 22), // "ui_read_from_file_u8_1"
QT_MOC_LITERAL(25, 549, 22), // "ui_read_from_file_u9_1"
QT_MOC_LITERAL(26, 572, 23), // "ui_read_from_file_u10_1"
QT_MOC_LITERAL(27, 596, 23), // "ui_read_from_file_u11_1"
QT_MOC_LITERAL(28, 620, 23), // "ui_read_from_file_u12_1"
QT_MOC_LITERAL(29, 644, 23), // "ui_read_from_file_u13_1"
QT_MOC_LITERAL(30, 668, 23), // "ui_read_from_file_u14_1"
QT_MOC_LITERAL(31, 692, 23), // "ui_read_from_file_u15_1"
QT_MOC_LITERAL(32, 716, 22), // "ui_read_from_file_t1_2"
QT_MOC_LITERAL(33, 739, 22), // "ui_read_from_file_t2_2"
QT_MOC_LITERAL(34, 762, 22), // "ui_read_from_file_t3_2"
QT_MOC_LITERAL(35, 785, 22), // "ui_read_from_file_t4_2"
QT_MOC_LITERAL(36, 808, 22), // "ui_read_from_file_t5_2"
QT_MOC_LITERAL(37, 831, 22), // "ui_read_from_file_t6_2"
QT_MOC_LITERAL(38, 854, 22), // "ui_read_from_file_t7_2"
QT_MOC_LITERAL(39, 877, 22), // "ui_read_from_file_t8_2"
QT_MOC_LITERAL(40, 900, 22), // "ui_read_from_file_t9_2"
QT_MOC_LITERAL(41, 923, 23), // "ui_read_from_file_t10_2"
QT_MOC_LITERAL(42, 947, 23), // "ui_read_from_file_t11_2"
QT_MOC_LITERAL(43, 971, 23), // "ui_read_from_file_t12_2"
QT_MOC_LITERAL(44, 995, 23), // "ui_read_from_file_t13_2"
QT_MOC_LITERAL(45, 1019, 23), // "ui_read_from_file_t14_2"
QT_MOC_LITERAL(46, 1043, 23), // "ui_read_from_file_t15_2"
QT_MOC_LITERAL(47, 1067, 22), // "ui_read_from_file_u1_2"
QT_MOC_LITERAL(48, 1090, 22), // "ui_read_from_file_u2_2"
QT_MOC_LITERAL(49, 1113, 22), // "ui_read_from_file_u3_2"
QT_MOC_LITERAL(50, 1136, 22), // "ui_read_from_file_u4_2"
QT_MOC_LITERAL(51, 1159, 22), // "ui_read_from_file_u5_2"
QT_MOC_LITERAL(52, 1182, 22), // "ui_read_from_file_u6_2"
QT_MOC_LITERAL(53, 1205, 22), // "ui_read_from_file_u7_2"
QT_MOC_LITERAL(54, 1228, 22), // "ui_read_from_file_u8_2"
QT_MOC_LITERAL(55, 1251, 22), // "ui_read_from_file_u9_2"
QT_MOC_LITERAL(56, 1274, 23), // "ui_read_from_file_u10_2"
QT_MOC_LITERAL(57, 1298, 23), // "ui_read_from_file_u11_2"
QT_MOC_LITERAL(58, 1322, 23), // "ui_read_from_file_u12_2"
QT_MOC_LITERAL(59, 1346, 23), // "ui_read_from_file_u13_2"
QT_MOC_LITERAL(60, 1370, 23), // "ui_read_from_file_u14_2"
QT_MOC_LITERAL(61, 1394, 23), // "ui_read_from_file_u15_2"
QT_MOC_LITERAL(62, 1418, 21), // "ui_write_to_file_t1_1"
QT_MOC_LITERAL(63, 1440, 21), // "ui_write_to_file_t1_2"
QT_MOC_LITERAL(64, 1462, 21), // "ui_write_to_file_t1_3"
QT_MOC_LITERAL(65, 1484, 21), // "ui_write_to_file_t1_4"
QT_MOC_LITERAL(66, 1506, 21), // "ui_write_to_file_t1_5"
QT_MOC_LITERAL(67, 1528, 21), // "ui_write_to_file_t1_6"
QT_MOC_LITERAL(68, 1550, 21), // "ui_write_to_file_t1_7"
QT_MOC_LITERAL(69, 1572, 21), // "ui_write_to_file_t1_8"
QT_MOC_LITERAL(70, 1594, 21), // "ui_write_to_file_t1_9"
QT_MOC_LITERAL(71, 1616, 22), // "ui_write_to_file_t1_10"
QT_MOC_LITERAL(72, 1639, 22), // "ui_write_to_file_t1_11"
QT_MOC_LITERAL(73, 1662, 22), // "ui_write_to_file_t1_12"
QT_MOC_LITERAL(74, 1685, 22), // "ui_write_to_file_t1_13"
QT_MOC_LITERAL(75, 1708, 22), // "ui_write_to_file_t1_14"
QT_MOC_LITERAL(76, 1731, 22), // "ui_write_to_file_t1_15"
QT_MOC_LITERAL(77, 1754, 21), // "ui_write_to_file_u1_1"
QT_MOC_LITERAL(78, 1776, 21), // "ui_write_to_file_u1_2"
QT_MOC_LITERAL(79, 1798, 21), // "ui_write_to_file_u1_3"
QT_MOC_LITERAL(80, 1820, 21), // "ui_write_to_file_u1_4"
QT_MOC_LITERAL(81, 1842, 21), // "ui_write_to_file_u1_5"
QT_MOC_LITERAL(82, 1864, 21), // "ui_write_to_file_u1_6"
QT_MOC_LITERAL(83, 1886, 21), // "ui_write_to_file_u1_7"
QT_MOC_LITERAL(84, 1908, 21), // "ui_write_to_file_u1_8"
QT_MOC_LITERAL(85, 1930, 21), // "ui_write_to_file_u1_9"
QT_MOC_LITERAL(86, 1952, 22), // "ui_write_to_file_u1_10"
QT_MOC_LITERAL(87, 1975, 22), // "ui_write_to_file_u1_11"
QT_MOC_LITERAL(88, 1998, 22), // "ui_write_to_file_u1_12"
QT_MOC_LITERAL(89, 2021, 22), // "ui_write_to_file_u1_13"
QT_MOC_LITERAL(90, 2044, 22), // "ui_write_to_file_u1_14"
QT_MOC_LITERAL(91, 2067, 22), // "ui_write_to_file_u1_15"
QT_MOC_LITERAL(92, 2090, 21), // "ui_write_to_file_t2_1"
QT_MOC_LITERAL(93, 2112, 21), // "ui_write_to_file_t2_2"
QT_MOC_LITERAL(94, 2134, 21), // "ui_write_to_file_t2_3"
QT_MOC_LITERAL(95, 2156, 21), // "ui_write_to_file_t2_4"
QT_MOC_LITERAL(96, 2178, 21), // "ui_write_to_file_t2_5"
QT_MOC_LITERAL(97, 2200, 21), // "ui_write_to_file_t2_6"
QT_MOC_LITERAL(98, 2222, 21), // "ui_write_to_file_t2_7"
QT_MOC_LITERAL(99, 2244, 21), // "ui_write_to_file_t2_8"
QT_MOC_LITERAL(100, 2266, 21), // "ui_write_to_file_t2_9"
QT_MOC_LITERAL(101, 2288, 22), // "ui_write_to_file_t2_10"
QT_MOC_LITERAL(102, 2311, 22), // "ui_write_to_file_t2_11"
QT_MOC_LITERAL(103, 2334, 22), // "ui_write_to_file_t2_12"
QT_MOC_LITERAL(104, 2357, 22), // "ui_write_to_file_t2_13"
QT_MOC_LITERAL(105, 2380, 22), // "ui_write_to_file_t2_14"
QT_MOC_LITERAL(106, 2403, 22), // "ui_write_to_file_t2_15"
QT_MOC_LITERAL(107, 2426, 21), // "ui_write_to_file_u2_1"
QT_MOC_LITERAL(108, 2448, 21), // "ui_write_to_file_u2_2"
QT_MOC_LITERAL(109, 2470, 21), // "ui_write_to_file_u2_3"
QT_MOC_LITERAL(110, 2492, 21), // "ui_write_to_file_u2_4"
QT_MOC_LITERAL(111, 2514, 21), // "ui_write_to_file_u2_5"
QT_MOC_LITERAL(112, 2536, 21), // "ui_write_to_file_u2_6"
QT_MOC_LITERAL(113, 2558, 21), // "ui_write_to_file_u2_7"
QT_MOC_LITERAL(114, 2580, 21), // "ui_write_to_file_u2_8"
QT_MOC_LITERAL(115, 2602, 21), // "ui_write_to_file_u2_9"
QT_MOC_LITERAL(116, 2624, 22), // "ui_write_to_file_u2_10"
QT_MOC_LITERAL(117, 2647, 22), // "ui_write_to_file_u2_11"
QT_MOC_LITERAL(118, 2670, 22), // "ui_write_to_file_u2_12"
QT_MOC_LITERAL(119, 2693, 22), // "ui_write_to_file_u2_13"
QT_MOC_LITERAL(120, 2716, 22), // "ui_write_to_file_u2_14"
QT_MOC_LITERAL(121, 2739, 22), // "ui_write_to_file_u2_15"
QT_MOC_LITERAL(122, 2762, 24) // "on_rewriteButton_clicked"

    },
    "time_voltage\0ui_read_from_file_t1_1\0"
    "\0ui_read_from_file_t2_1\0ui_read_from_file_t3_1\0"
    "ui_read_from_file_t4_1\0ui_read_from_file_t5_1\0"
    "ui_read_from_file_t6_1\0ui_read_from_file_t7_1\0"
    "ui_read_from_file_t8_1\0ui_read_from_file_t9_1\0"
    "ui_read_from_file_t10_1\0ui_read_from_file_t11_1\0"
    "ui_read_from_file_t12_1\0ui_read_from_file_t13_1\0"
    "ui_read_from_file_t14_1\0ui_read_from_file_t15_1\0"
    "ui_read_from_file_u1_1\0ui_read_from_file_u2_1\0"
    "ui_read_from_file_u3_1\0ui_read_from_file_u4_1\0"
    "ui_read_from_file_u5_1\0ui_read_from_file_u6_1\0"
    "ui_read_from_file_u7_1\0ui_read_from_file_u8_1\0"
    "ui_read_from_file_u9_1\0ui_read_from_file_u10_1\0"
    "ui_read_from_file_u11_1\0ui_read_from_file_u12_1\0"
    "ui_read_from_file_u13_1\0ui_read_from_file_u14_1\0"
    "ui_read_from_file_u15_1\0ui_read_from_file_t1_2\0"
    "ui_read_from_file_t2_2\0ui_read_from_file_t3_2\0"
    "ui_read_from_file_t4_2\0ui_read_from_file_t5_2\0"
    "ui_read_from_file_t6_2\0ui_read_from_file_t7_2\0"
    "ui_read_from_file_t8_2\0ui_read_from_file_t9_2\0"
    "ui_read_from_file_t10_2\0ui_read_from_file_t11_2\0"
    "ui_read_from_file_t12_2\0ui_read_from_file_t13_2\0"
    "ui_read_from_file_t14_2\0ui_read_from_file_t15_2\0"
    "ui_read_from_file_u1_2\0ui_read_from_file_u2_2\0"
    "ui_read_from_file_u3_2\0ui_read_from_file_u4_2\0"
    "ui_read_from_file_u5_2\0ui_read_from_file_u6_2\0"
    "ui_read_from_file_u7_2\0ui_read_from_file_u8_2\0"
    "ui_read_from_file_u9_2\0ui_read_from_file_u10_2\0"
    "ui_read_from_file_u11_2\0ui_read_from_file_u12_2\0"
    "ui_read_from_file_u13_2\0ui_read_from_file_u14_2\0"
    "ui_read_from_file_u15_2\0ui_write_to_file_t1_1\0"
    "ui_write_to_file_t1_2\0ui_write_to_file_t1_3\0"
    "ui_write_to_file_t1_4\0ui_write_to_file_t1_5\0"
    "ui_write_to_file_t1_6\0ui_write_to_file_t1_7\0"
    "ui_write_to_file_t1_8\0ui_write_to_file_t1_9\0"
    "ui_write_to_file_t1_10\0ui_write_to_file_t1_11\0"
    "ui_write_to_file_t1_12\0ui_write_to_file_t1_13\0"
    "ui_write_to_file_t1_14\0ui_write_to_file_t1_15\0"
    "ui_write_to_file_u1_1\0ui_write_to_file_u1_2\0"
    "ui_write_to_file_u1_3\0ui_write_to_file_u1_4\0"
    "ui_write_to_file_u1_5\0ui_write_to_file_u1_6\0"
    "ui_write_to_file_u1_7\0ui_write_to_file_u1_8\0"
    "ui_write_to_file_u1_9\0ui_write_to_file_u1_10\0"
    "ui_write_to_file_u1_11\0ui_write_to_file_u1_12\0"
    "ui_write_to_file_u1_13\0ui_write_to_file_u1_14\0"
    "ui_write_to_file_u1_15\0ui_write_to_file_t2_1\0"
    "ui_write_to_file_t2_2\0ui_write_to_file_t2_3\0"
    "ui_write_to_file_t2_4\0ui_write_to_file_t2_5\0"
    "ui_write_to_file_t2_6\0ui_write_to_file_t2_7\0"
    "ui_write_to_file_t2_8\0ui_write_to_file_t2_9\0"
    "ui_write_to_file_t2_10\0ui_write_to_file_t2_11\0"
    "ui_write_to_file_t2_12\0ui_write_to_file_t2_13\0"
    "ui_write_to_file_t2_14\0ui_write_to_file_t2_15\0"
    "ui_write_to_file_u2_1\0ui_write_to_file_u2_2\0"
    "ui_write_to_file_u2_3\0ui_write_to_file_u2_4\0"
    "ui_write_to_file_u2_5\0ui_write_to_file_u2_6\0"
    "ui_write_to_file_u2_7\0ui_write_to_file_u2_8\0"
    "ui_write_to_file_u2_9\0ui_write_to_file_u2_10\0"
    "ui_write_to_file_u2_11\0ui_write_to_file_u2_12\0"
    "ui_write_to_file_u2_13\0ui_write_to_file_u2_14\0"
    "ui_write_to_file_u2_15\0on_rewriteButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_time_voltage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
     121,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  619,    2, 0x0a /* Public */,
       3,    0,  620,    2, 0x0a /* Public */,
       4,    0,  621,    2, 0x0a /* Public */,
       5,    0,  622,    2, 0x0a /* Public */,
       6,    0,  623,    2, 0x0a /* Public */,
       7,    0,  624,    2, 0x0a /* Public */,
       8,    0,  625,    2, 0x0a /* Public */,
       9,    0,  626,    2, 0x0a /* Public */,
      10,    0,  627,    2, 0x0a /* Public */,
      11,    0,  628,    2, 0x0a /* Public */,
      12,    0,  629,    2, 0x0a /* Public */,
      13,    0,  630,    2, 0x0a /* Public */,
      14,    0,  631,    2, 0x0a /* Public */,
      15,    0,  632,    2, 0x0a /* Public */,
      16,    0,  633,    2, 0x0a /* Public */,
      17,    0,  634,    2, 0x0a /* Public */,
      18,    0,  635,    2, 0x0a /* Public */,
      19,    0,  636,    2, 0x0a /* Public */,
      20,    0,  637,    2, 0x0a /* Public */,
      21,    0,  638,    2, 0x0a /* Public */,
      22,    0,  639,    2, 0x0a /* Public */,
      23,    0,  640,    2, 0x0a /* Public */,
      24,    0,  641,    2, 0x0a /* Public */,
      25,    0,  642,    2, 0x0a /* Public */,
      26,    0,  643,    2, 0x0a /* Public */,
      27,    0,  644,    2, 0x0a /* Public */,
      28,    0,  645,    2, 0x0a /* Public */,
      29,    0,  646,    2, 0x0a /* Public */,
      30,    0,  647,    2, 0x0a /* Public */,
      31,    0,  648,    2, 0x0a /* Public */,
      32,    0,  649,    2, 0x0a /* Public */,
      33,    0,  650,    2, 0x0a /* Public */,
      34,    0,  651,    2, 0x0a /* Public */,
      35,    0,  652,    2, 0x0a /* Public */,
      36,    0,  653,    2, 0x0a /* Public */,
      37,    0,  654,    2, 0x0a /* Public */,
      38,    0,  655,    2, 0x0a /* Public */,
      39,    0,  656,    2, 0x0a /* Public */,
      40,    0,  657,    2, 0x0a /* Public */,
      41,    0,  658,    2, 0x0a /* Public */,
      42,    0,  659,    2, 0x0a /* Public */,
      43,    0,  660,    2, 0x0a /* Public */,
      44,    0,  661,    2, 0x0a /* Public */,
      45,    0,  662,    2, 0x0a /* Public */,
      46,    0,  663,    2, 0x0a /* Public */,
      47,    0,  664,    2, 0x0a /* Public */,
      48,    0,  665,    2, 0x0a /* Public */,
      49,    0,  666,    2, 0x0a /* Public */,
      50,    0,  667,    2, 0x0a /* Public */,
      51,    0,  668,    2, 0x0a /* Public */,
      52,    0,  669,    2, 0x0a /* Public */,
      53,    0,  670,    2, 0x0a /* Public */,
      54,    0,  671,    2, 0x0a /* Public */,
      55,    0,  672,    2, 0x0a /* Public */,
      56,    0,  673,    2, 0x0a /* Public */,
      57,    0,  674,    2, 0x0a /* Public */,
      58,    0,  675,    2, 0x0a /* Public */,
      59,    0,  676,    2, 0x0a /* Public */,
      60,    0,  677,    2, 0x0a /* Public */,
      61,    0,  678,    2, 0x0a /* Public */,
      62,    0,  679,    2, 0x0a /* Public */,
      63,    0,  680,    2, 0x0a /* Public */,
      64,    0,  681,    2, 0x0a /* Public */,
      65,    0,  682,    2, 0x0a /* Public */,
      66,    0,  683,    2, 0x0a /* Public */,
      67,    0,  684,    2, 0x0a /* Public */,
      68,    0,  685,    2, 0x0a /* Public */,
      69,    0,  686,    2, 0x0a /* Public */,
      70,    0,  687,    2, 0x0a /* Public */,
      71,    0,  688,    2, 0x0a /* Public */,
      72,    0,  689,    2, 0x0a /* Public */,
      73,    0,  690,    2, 0x0a /* Public */,
      74,    0,  691,    2, 0x0a /* Public */,
      75,    0,  692,    2, 0x0a /* Public */,
      76,    0,  693,    2, 0x0a /* Public */,
      77,    0,  694,    2, 0x0a /* Public */,
      78,    0,  695,    2, 0x0a /* Public */,
      79,    0,  696,    2, 0x0a /* Public */,
      80,    0,  697,    2, 0x0a /* Public */,
      81,    0,  698,    2, 0x0a /* Public */,
      82,    0,  699,    2, 0x0a /* Public */,
      83,    0,  700,    2, 0x0a /* Public */,
      84,    0,  701,    2, 0x0a /* Public */,
      85,    0,  702,    2, 0x0a /* Public */,
      86,    0,  703,    2, 0x0a /* Public */,
      87,    0,  704,    2, 0x0a /* Public */,
      88,    0,  705,    2, 0x0a /* Public */,
      89,    0,  706,    2, 0x0a /* Public */,
      90,    0,  707,    2, 0x0a /* Public */,
      91,    0,  708,    2, 0x0a /* Public */,
      92,    0,  709,    2, 0x0a /* Public */,
      93,    0,  710,    2, 0x0a /* Public */,
      94,    0,  711,    2, 0x0a /* Public */,
      95,    0,  712,    2, 0x0a /* Public */,
      96,    0,  713,    2, 0x0a /* Public */,
      97,    0,  714,    2, 0x0a /* Public */,
      98,    0,  715,    2, 0x0a /* Public */,
      99,    0,  716,    2, 0x0a /* Public */,
     100,    0,  717,    2, 0x0a /* Public */,
     101,    0,  718,    2, 0x0a /* Public */,
     102,    0,  719,    2, 0x0a /* Public */,
     103,    0,  720,    2, 0x0a /* Public */,
     104,    0,  721,    2, 0x0a /* Public */,
     105,    0,  722,    2, 0x0a /* Public */,
     106,    0,  723,    2, 0x0a /* Public */,
     107,    0,  724,    2, 0x0a /* Public */,
     108,    0,  725,    2, 0x0a /* Public */,
     109,    0,  726,    2, 0x0a /* Public */,
     110,    0,  727,    2, 0x0a /* Public */,
     111,    0,  728,    2, 0x0a /* Public */,
     112,    0,  729,    2, 0x0a /* Public */,
     113,    0,  730,    2, 0x0a /* Public */,
     114,    0,  731,    2, 0x0a /* Public */,
     115,    0,  732,    2, 0x0a /* Public */,
     116,    0,  733,    2, 0x0a /* Public */,
     117,    0,  734,    2, 0x0a /* Public */,
     118,    0,  735,    2, 0x0a /* Public */,
     119,    0,  736,    2, 0x0a /* Public */,
     120,    0,  737,    2, 0x0a /* Public */,
     121,    0,  738,    2, 0x0a /* Public */,
     122,    0,  739,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void time_voltage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<time_voltage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->ui_read_from_file_t1_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 1: { QString _r = _t->ui_read_from_file_t2_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 2: { QString _r = _t->ui_read_from_file_t3_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 3: { QString _r = _t->ui_read_from_file_t4_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 4: { QString _r = _t->ui_read_from_file_t5_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 5: { QString _r = _t->ui_read_from_file_t6_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 6: { QString _r = _t->ui_read_from_file_t7_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 7: { QString _r = _t->ui_read_from_file_t8_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 8: { QString _r = _t->ui_read_from_file_t9_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 9: { QString _r = _t->ui_read_from_file_t10_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 10: { QString _r = _t->ui_read_from_file_t11_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 11: { QString _r = _t->ui_read_from_file_t12_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 12: { QString _r = _t->ui_read_from_file_t13_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 13: { QString _r = _t->ui_read_from_file_t14_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 14: { QString _r = _t->ui_read_from_file_t15_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 15: { QString _r = _t->ui_read_from_file_u1_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 16: { QString _r = _t->ui_read_from_file_u2_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 17: { QString _r = _t->ui_read_from_file_u3_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 18: { QString _r = _t->ui_read_from_file_u4_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 19: { QString _r = _t->ui_read_from_file_u5_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 20: { QString _r = _t->ui_read_from_file_u6_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 21: { QString _r = _t->ui_read_from_file_u7_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 22: { QString _r = _t->ui_read_from_file_u8_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 23: { QString _r = _t->ui_read_from_file_u9_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 24: { QString _r = _t->ui_read_from_file_u10_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 25: { QString _r = _t->ui_read_from_file_u11_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 26: { QString _r = _t->ui_read_from_file_u12_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 27: { QString _r = _t->ui_read_from_file_u13_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 28: { QString _r = _t->ui_read_from_file_u14_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 29: { QString _r = _t->ui_read_from_file_u15_1();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 30: { QString _r = _t->ui_read_from_file_t1_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 31: { QString _r = _t->ui_read_from_file_t2_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 32: { QString _r = _t->ui_read_from_file_t3_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 33: { QString _r = _t->ui_read_from_file_t4_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 34: { QString _r = _t->ui_read_from_file_t5_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 35: { QString _r = _t->ui_read_from_file_t6_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 36: { QString _r = _t->ui_read_from_file_t7_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 37: { QString _r = _t->ui_read_from_file_t8_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 38: { QString _r = _t->ui_read_from_file_t9_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 39: { QString _r = _t->ui_read_from_file_t10_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 40: { QString _r = _t->ui_read_from_file_t11_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 41: { QString _r = _t->ui_read_from_file_t12_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 42: { QString _r = _t->ui_read_from_file_t13_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 43: { QString _r = _t->ui_read_from_file_t14_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 44: { QString _r = _t->ui_read_from_file_t15_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 45: { QString _r = _t->ui_read_from_file_u1_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 46: { QString _r = _t->ui_read_from_file_u2_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 47: { QString _r = _t->ui_read_from_file_u3_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 48: { QString _r = _t->ui_read_from_file_u4_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 49: { QString _r = _t->ui_read_from_file_u5_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 50: { QString _r = _t->ui_read_from_file_u6_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 51: { QString _r = _t->ui_read_from_file_u7_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 52: { QString _r = _t->ui_read_from_file_u8_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 53: { QString _r = _t->ui_read_from_file_u9_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 54: { QString _r = _t->ui_read_from_file_u10_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 55: { QString _r = _t->ui_read_from_file_u11_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 56: { QString _r = _t->ui_read_from_file_u12_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 57: { QString _r = _t->ui_read_from_file_u13_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 58: { QString _r = _t->ui_read_from_file_u14_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 59: { QString _r = _t->ui_read_from_file_u15_2();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 60: _t->ui_write_to_file_t1_1(); break;
        case 61: _t->ui_write_to_file_t1_2(); break;
        case 62: _t->ui_write_to_file_t1_3(); break;
        case 63: _t->ui_write_to_file_t1_4(); break;
        case 64: _t->ui_write_to_file_t1_5(); break;
        case 65: _t->ui_write_to_file_t1_6(); break;
        case 66: _t->ui_write_to_file_t1_7(); break;
        case 67: _t->ui_write_to_file_t1_8(); break;
        case 68: _t->ui_write_to_file_t1_9(); break;
        case 69: _t->ui_write_to_file_t1_10(); break;
        case 70: _t->ui_write_to_file_t1_11(); break;
        case 71: _t->ui_write_to_file_t1_12(); break;
        case 72: _t->ui_write_to_file_t1_13(); break;
        case 73: _t->ui_write_to_file_t1_14(); break;
        case 74: _t->ui_write_to_file_t1_15(); break;
        case 75: _t->ui_write_to_file_u1_1(); break;
        case 76: _t->ui_write_to_file_u1_2(); break;
        case 77: _t->ui_write_to_file_u1_3(); break;
        case 78: _t->ui_write_to_file_u1_4(); break;
        case 79: _t->ui_write_to_file_u1_5(); break;
        case 80: _t->ui_write_to_file_u1_6(); break;
        case 81: _t->ui_write_to_file_u1_7(); break;
        case 82: _t->ui_write_to_file_u1_8(); break;
        case 83: _t->ui_write_to_file_u1_9(); break;
        case 84: _t->ui_write_to_file_u1_10(); break;
        case 85: _t->ui_write_to_file_u1_11(); break;
        case 86: _t->ui_write_to_file_u1_12(); break;
        case 87: _t->ui_write_to_file_u1_13(); break;
        case 88: _t->ui_write_to_file_u1_14(); break;
        case 89: _t->ui_write_to_file_u1_15(); break;
        case 90: _t->ui_write_to_file_t2_1(); break;
        case 91: _t->ui_write_to_file_t2_2(); break;
        case 92: _t->ui_write_to_file_t2_3(); break;
        case 93: _t->ui_write_to_file_t2_4(); break;
        case 94: _t->ui_write_to_file_t2_5(); break;
        case 95: _t->ui_write_to_file_t2_6(); break;
        case 96: _t->ui_write_to_file_t2_7(); break;
        case 97: _t->ui_write_to_file_t2_8(); break;
        case 98: _t->ui_write_to_file_t2_9(); break;
        case 99: _t->ui_write_to_file_t2_10(); break;
        case 100: _t->ui_write_to_file_t2_11(); break;
        case 101: _t->ui_write_to_file_t2_12(); break;
        case 102: _t->ui_write_to_file_t2_13(); break;
        case 103: _t->ui_write_to_file_t2_14(); break;
        case 104: _t->ui_write_to_file_t2_15(); break;
        case 105: _t->ui_write_to_file_u2_1(); break;
        case 106: _t->ui_write_to_file_u2_2(); break;
        case 107: _t->ui_write_to_file_u2_3(); break;
        case 108: _t->ui_write_to_file_u2_4(); break;
        case 109: _t->ui_write_to_file_u2_5(); break;
        case 110: _t->ui_write_to_file_u2_6(); break;
        case 111: _t->ui_write_to_file_u2_7(); break;
        case 112: _t->ui_write_to_file_u2_8(); break;
        case 113: _t->ui_write_to_file_u2_9(); break;
        case 114: _t->ui_write_to_file_u2_10(); break;
        case 115: _t->ui_write_to_file_u2_11(); break;
        case 116: _t->ui_write_to_file_u2_12(); break;
        case 117: _t->ui_write_to_file_u2_13(); break;
        case 118: _t->ui_write_to_file_u2_14(); break;
        case 119: _t->ui_write_to_file_u2_15(); break;
        case 120: _t->on_rewriteButton_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject time_voltage::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_time_voltage.data,
    qt_meta_data_time_voltage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *time_voltage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *time_voltage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_time_voltage.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int time_voltage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 121)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 121;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 121)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 121;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
