/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QPushButton *startStopCycleButton;
    QPushButton *b_kButton;
    QPushButton *coefficientsButton;
    QPushButton *timeVoltageButton;
    QPushButton *graphButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(762, 358);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(centralwidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 740, 294));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(10);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(5, 5, 5, 5);
        startStopCycleButton = new QPushButton(scrollAreaWidgetContents);
        startStopCycleButton->setObjectName(QString::fromUtf8("startStopCycleButton"));
        QFont font;
        font.setPointSize(18);
        startStopCycleButton->setFont(font);

        gridLayout_2->addWidget(startStopCycleButton, 0, 0, 1, 1);

        b_kButton = new QPushButton(scrollAreaWidgetContents);
        b_kButton->setObjectName(QString::fromUtf8("b_kButton"));
        b_kButton->setFont(font);

        gridLayout_2->addWidget(b_kButton, 3, 0, 1, 1);

        coefficientsButton = new QPushButton(scrollAreaWidgetContents);
        coefficientsButton->setObjectName(QString::fromUtf8("coefficientsButton"));
        coefficientsButton->setFont(font);

        gridLayout_2->addWidget(coefficientsButton, 1, 0, 1, 1);

        timeVoltageButton = new QPushButton(scrollAreaWidgetContents);
        timeVoltageButton->setObjectName(QString::fromUtf8("timeVoltageButton"));
        timeVoltageButton->setFont(font);

        gridLayout_2->addWidget(timeVoltageButton, 2, 0, 1, 1);

        graphButton = new QPushButton(scrollAreaWidgetContents);
        graphButton->setObjectName(QString::fromUtf8("graphButton"));
        graphButton->setFont(font);

        gridLayout_2->addWidget(graphButton, 4, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 762, 20));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        startStopCycleButton->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\276\320\272\320\275\320\276 \320\267\320\260\320\277\321\203\321\201\320\272\320\260 \321\206\320\270\320\272\320\273\320\260", nullptr));
        b_kButton->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\276\320\272\320\275\320\276 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217 B0 \320\270 K", nullptr));
        coefficientsButton->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\276\320\272\320\275\320\276 \320\272\320\276\321\215\321\204\321\204\320\270\321\206\320\270\320\265\320\275\321\202\320\276\320\262 \320\272\320\276\321\200\321\200\320\265\320\272\321\206\320\270\320\270", nullptr));
        timeVoltageButton->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\276\320\272\320\275\320\276 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217 \320\262\321\200\320\265\320\274\320\265\320\275\320\270 \320\270 \320\275\320\260\320\277\321\200\321\217\320\266\320\265\320\275\320\270\320\271", nullptr));
        graphButton->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\263\321\200\320\260\321\204\320\270\320\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
