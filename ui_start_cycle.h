/********************************************************************************
** Form generated from reading UI file 'start_cycle.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_START_CYCLE_H
#define UI_START_CYCLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_start_cycle
{
public:
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QPushButton *startButton;
    QPushButton *stopButton;

    void setupUi(QWidget *start_cycle)
    {
        if (start_cycle->objectName().isEmpty())
            start_cycle->setObjectName(QString::fromUtf8("start_cycle"));
        start_cycle->resize(476, 290);
        gridLayout = new QGridLayout(start_cycle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(start_cycle);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 454, 268));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        startButton = new QPushButton(scrollAreaWidgetContents);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        QFont font;
        font.setPointSize(18);
        startButton->setFont(font);

        gridLayout_2->addWidget(startButton, 0, 0, 1, 1);

        stopButton = new QPushButton(scrollAreaWidgetContents);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));
        stopButton->setFont(font);

        gridLayout_2->addWidget(stopButton, 0, 1, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);


        retranslateUi(start_cycle);

        QMetaObject::connectSlotsByName(start_cycle);
    } // setupUi

    void retranslateUi(QWidget *start_cycle)
    {
        start_cycle->setWindowTitle(QCoreApplication::translate("start_cycle", "Form", nullptr));
        startButton->setText(QCoreApplication::translate("start_cycle", "\320\227\320\260\320\277\321\203\321\201\320\272 \321\206\320\270\320\272\320\273\320\260", nullptr));
        stopButton->setText(QCoreApplication::translate("start_cycle", "\320\227\320\260\320\262\320\265\321\200\321\210\320\265\320\275\320\270\320\265 \321\206\320\270\320\272\320\273\320\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class start_cycle: public Ui_start_cycle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_START_CYCLE_H
