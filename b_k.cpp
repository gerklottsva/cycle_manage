#include "b_k.h"

std::vector<std::string> res_cycle;
std::string find_B0 = "B0";
std::string find_K = "K";
std::string path_to_cycle = "D:\\cycle.py";

void Read_File_Cycle()
{
    res_cycle.clear();
    std::fstream file_Cycle;
    file_Cycle.open(path_to_cycle, std::fstream::in);

    std::string textLine;
    while (getline(file_Cycle, textLine))
    {
        res_cycle.push_back(textLine);
    }

    file_Cycle.close();
}

QString Read_K()
{
        auto it_K = std::find_if(res_cycle.begin(), res_cycle.end(), [&](std::string& var)
            {
                return var.find(find_K) != std::string::npos;
            });

        return(QString::fromStdString(*it_K));
}

QString Read_B0()
{
        auto it_B0 = std::find_if(res_cycle.begin(), res_cycle.end(), [&](std::string& var)
            {
                return var.find(find_B0) != std::string::npos;
            });

        return(QString::fromStdString(*it_B0));
}

void Write_B0(double value)
{
        auto it_B0 = std::find_if(res_cycle.begin(), res_cycle.end(), [&](std::string& var)
            {
                return var.find(find_B0) != std::string::npos;
            });

        if (it_B0 != res_cycle.end()) (it_B0)->clear();

        std::stringstream string_B0;
        string_B0 << "B0 = " <<"\""<< value<< "\"";
        *it_B0 = string_B0.str();
}

void Write_K(double value)
{
        auto it_K = std::find_if(res_cycle.begin(), res_cycle.end(), [&](std::string& var)
            {
                return var.find(find_K) != std::string::npos;
            });

        if (it_K != res_cycle.end()) (it_K)->clear();

        std::stringstream string_K;
        string_K << "K = " <<"\""<< value<< "\"";
        *it_K = string_K.str();
}

void Write_To_File_Cycle()
{
    std::fstream file_Cycle;
    file_Cycle.open(path_to_cycle, std::fstream::out | std::ofstream::trunc);
    if (file_Cycle.is_open())
    {
    int k = 0;
        for (auto& var : res_cycle)
      {
         if (k++ > 0)  file_Cycle << std::endl;
         file_Cycle << var;
      }
    }
    file_Cycle.close();
}

b_k::b_k(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::b_k)
{
    ui->setupUi(this);

    setWindowTitle("Изменение B0 и K");

    Read_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
    ui->readed_value_K->setText(Read_K());
    connect(ui->writeButtonB0, SIGNAL(clicked()), this, SLOT(ui_write_to_file_B0()));
    connect(ui->writeButtonK, SIGNAL(clicked()), this, SLOT(ui_write_to_file_K()));
    connect(ui->plusButtonB0, SIGNAL(clicked()), this, SLOT(plus_value_B0()));
    connect(ui->minusButtonB0, SIGNAL(clicked()), this, SLOT(minus_value_B0()));
    connect(ui->oneplusButtonB0, SIGNAL(clicked()), this, SLOT(oneplus_value_B0()));
    connect(ui->oneminusButtonB0, SIGNAL(clicked()), this, SLOT(oneminus_value_B0()));
    connect(ui->twoplusButtonB0, SIGNAL(clicked()), this, SLOT(twoplus_value_B0()));
    connect(ui->twominusButtonB0, SIGNAL(clicked()), this, SLOT(twominus_value_B0()));
    connect(ui->threeplusButtonB0, SIGNAL(clicked()), this, SLOT(threeplus_value_B0()));
    connect(ui->threeminusButtonB0, SIGNAL(clicked()), this, SLOT(threeminus_value_B0()));
    connect(ui->plusButtonK, SIGNAL(clicked()), this, SLOT(plus_value_K()));
    connect(ui->minusButtonK, SIGNAL(clicked()), this, SLOT(minus_value_K()));
    connect(ui->oneplusButtonK, SIGNAL(clicked()), this, SLOT(oneplus_value_K()));
    connect(ui->oneminusButtonK, SIGNAL(clicked()), this, SLOT(oneminus_value_K()));
    connect(ui->twoplusButtonK, SIGNAL(clicked()), this, SLOT(twoplus_value_K()));
    connect(ui->twominusButtonK, SIGNAL(clicked()), this, SLOT(twominus_value_K()));
    connect(ui->threeplusButtonK, SIGNAL(clicked()), this, SLOT(threeplus_value_K()));
    connect(ui->threeminusButtonK, SIGNAL(clicked()), this, SLOT(threeminus_value_K()));
}

b_k::~b_k()
{
    delete ui;
}

void b_k::ui_write_to_file_B0()
{
    QString qstr;
    Read_File_Cycle();
    qstr = ui->writed_value_B0->text();

    bool b;
    int c = 0;

    if(qstr.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr.size();i++)
    {
        if(!qstr[i].isNumber())
        {
            if(qstr[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr.at(qstr.size()-1)!='.'&&qstr.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        Write_B0(qstr.toDouble());
        Write_To_File_Cycle();
        ui->readed_value_B0->setText(Read_B0());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
}

void b_k::ui_write_to_file_K()
{
    QString qstr;
    Read_File_Cycle();
    qstr = ui->writed_value_K->text();

    bool b;
    int c = 0;

    if(qstr.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr.size();i++)
    {
        if(!qstr[i].isNumber())
        {
            if(qstr[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr.at(qstr.size()-1)!='.'&&qstr.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        Write_K(qstr.toDouble());
        Write_To_File_Cycle();
        ui->readed_value_K->setText(Read_K());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
}

void b_k::plus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()+1);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::minus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()-1);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::oneplus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()+0.1);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::oneminus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()-0.1);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::twoplus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()+0.01);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::twominus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()-0.01);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::threeplus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()+0.001);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::threeminus_value_B0()
{
    QString str = ui->readed_value_B0->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_B0(str.toDouble()-0.001);
    Write_To_File_Cycle();
    ui->readed_value_B0->setText(Read_B0());
}

void b_k::plus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()+1);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::minus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()-1);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::oneplus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()+0.1);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::oneminus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()-0.1);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::twoplus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()+0.01);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::twominus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()-0.01);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::threeplus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()+0.001);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}

void b_k::threeminus_value_K()
{
    QString str = ui->readed_value_K->text();
    str.remove(0,5);
    str.remove(QChar('"'));
    Write_K(str.toDouble()-0.001);
    Write_To_File_Cycle();
    ui->readed_value_K->setText(Read_K());
}
