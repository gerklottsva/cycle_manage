/********************************************************************************
** Form generated from reading UI file 'coefficients.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COEFFICIENTS_H
#define UI_COEFFICIENTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_coefficients
{
public:
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QPushButton *coefButton;
    QLabel *label_e;
    QComboBox *cbxElements;
    QLabel *label_a_n_e;
    QLabel *label_c_n_e;
    QLabel *label_b_n_e;

    void setupUi(QWidget *coefficients)
    {
        if (coefficients->objectName().isEmpty())
            coefficients->setObjectName(QString::fromUtf8("coefficients"));
        coefficients->resize(1093, 537);
        gridLayout = new QGridLayout(coefficients);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(coefficients);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1071, 515));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(10);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(5, 5, 5, 5);
        coefButton = new QPushButton(scrollAreaWidgetContents);
        coefButton->setObjectName(QString::fromUtf8("coefButton"));
        QFont font;
        font.setPointSize(19);
        coefButton->setFont(font);

        gridLayout_2->addWidget(coefButton, 1, 0, 1, 4);

        label_e = new QLabel(scrollAreaWidgetContents);
        label_e->setObjectName(QString::fromUtf8("label_e"));

        gridLayout_2->addWidget(label_e, 0, 0, 1, 2);

        cbxElements = new QComboBox(scrollAreaWidgetContents);
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->addItem(QString());
        cbxElements->setObjectName(QString::fromUtf8("cbxElements"));
        QFont font1;
        font1.setPointSize(18);
        cbxElements->setFont(font1);

        gridLayout_2->addWidget(cbxElements, 0, 2, 1, 2);

        label_a_n_e = new QLabel(scrollAreaWidgetContents);
        label_a_n_e->setObjectName(QString::fromUtf8("label_a_n_e"));
        label_a_n_e->setFont(font1);

        gridLayout_2->addWidget(label_a_n_e, 2, 0, 1, 1);

        label_c_n_e = new QLabel(scrollAreaWidgetContents);
        label_c_n_e->setObjectName(QString::fromUtf8("label_c_n_e"));
        label_c_n_e->setFont(font1);

        gridLayout_2->addWidget(label_c_n_e, 2, 3, 1, 1);

        label_b_n_e = new QLabel(scrollAreaWidgetContents);
        label_b_n_e->setObjectName(QString::fromUtf8("label_b_n_e"));
        label_b_n_e->setFont(font1);

        gridLayout_2->addWidget(label_b_n_e, 2, 1, 1, 2);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);


        retranslateUi(coefficients);

        QMetaObject::connectSlotsByName(coefficients);
    } // setupUi

    void retranslateUi(QWidget *coefficients)
    {
        coefficients->setWindowTitle(QCoreApplication::translate("coefficients", "Form", nullptr));
        coefButton->setText(QCoreApplication::translate("coefficients", "\320\240\320\260\321\201\321\201\321\207\320\270\321\202\320\260\321\202\321\214 \320\272\320\276\321\215\321\204\321\204\320\270\321\206\320\270\320\265\321\202\321\213 \320\272\320\276\321\200\321\200\320\265\320\272\321\206\320\270\320\270", nullptr));
        label_e->setText(QCoreApplication::translate("coefficients", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\242\320\270\320\277 \321\203\321\201\320\272\320\276\321\200\321\217\320\265\320\274\321\213\321\205 \321\207\320\260\321\201\321\202\320\270\321\206:</span></p></body></html>", nullptr));
        cbxElements->setItemText(0, QCoreApplication::translate("coefficients", "\320\237\321\200\320\276\321\202\320\276\320\275\321\213 (1+_1)", nullptr));
        cbxElements->setItemText(1, QCoreApplication::translate("coefficients", "\320\224\320\265\320\271\321\202\320\276\320\275\321\213 (1+_2)", nullptr));
        cbxElements->setItemText(2, QCoreApplication::translate("coefficients", "\320\223\320\265\320\273\320\270\320\271 (1+_4)", nullptr));
        cbxElements->setItemText(3, QCoreApplication::translate("coefficients", "\320\223\320\265\320\273\320\270\320\271 (2+_3)", nullptr));
        cbxElements->setItemText(4, QCoreApplication::translate("coefficients", "\320\223\320\265\320\273\320\270\320\271 (2+_4)", nullptr));
        cbxElements->setItemText(5, QCoreApplication::translate("coefficients", "\320\241\320\265\321\200\320\260 (16+_32)", nullptr));
        cbxElements->setItemText(6, QCoreApplication::translate("coefficients", "\320\232\321\200\320\265\320\274\320\275\320\270\320\271 (14+_28)", nullptr));
        cbxElements->setItemText(7, QCoreApplication::translate("coefficients", "\320\234\320\260\320\263\320\275\320\270\320\271 (12+_24)", nullptr));
        cbxElements->setItemText(8, QCoreApplication::translate("coefficients", "\320\232\320\270\321\201\320\273\320\276\321\200\320\276\320\264  (8+_16)", nullptr));
        cbxElements->setItemText(9, QCoreApplication::translate("coefficients", "\320\243\320\263\320\273\320\265\321\200\320\276\320\264 (6+_12)", nullptr));
        cbxElements->setItemText(10, QCoreApplication::translate("coefficients", "\320\233\320\270\321\202\320\270\320\271 (3+_6)", nullptr));
        cbxElements->setItemText(11, QCoreApplication::translate("coefficients", "\320\233\320\270\321\202\320\270\320\271 (3+_7)", nullptr));
        cbxElements->setItemText(12, QCoreApplication::translate("coefficients", "\320\245\320\273\320\276\321\200 (17+_35)", nullptr));
        cbxElements->setItemText(13, QCoreApplication::translate("coefficients", "\320\234\320\260\320\263\320\275\320\270\320\271 (12+_25)", nullptr));
        cbxElements->setItemText(14, QCoreApplication::translate("coefficients", "\320\244\321\202\320\276\321\200 (9+_19)", nullptr));
        cbxElements->setItemText(15, QCoreApplication::translate("coefficients", "\320\234\320\260\320\263\320\275\320\270\320\271 (12+_26)", nullptr));
        cbxElements->setItemText(16, QCoreApplication::translate("coefficients", "\320\235\320\265\320\276\320\275 (10+_22)", nullptr));
        cbxElements->setItemText(17, QCoreApplication::translate("coefficients", "\320\220\321\200\320\263\320\276\320\275 (18+_40)", nullptr));
        cbxElements->setItemText(18, QCoreApplication::translate("coefficients", "\320\232\321\200\320\270\320\277\321\202\320\276\320\275 (36+_84)", nullptr));
        cbxElements->setItemText(19, QCoreApplication::translate("coefficients", "\320\220\321\200\320\263\320\276\320\275 (16+_40)", nullptr));
        cbxElements->setItemText(20, QCoreApplication::translate("coefficients", "\320\232\321\200\320\270\320\277\321\202\320\276\320\275 (30+_84)", nullptr));
        cbxElements->setItemText(21, QCoreApplication::translate("coefficients", "\320\220\321\200\320\263\320\276\320\275 (14+_40)", nullptr));
        cbxElements->setItemText(22, QCoreApplication::translate("coefficients", "\320\232\321\200\320\270\320\277\321\202\320\276\320\275 (29+_84)", nullptr));
        cbxElements->setItemText(23, QCoreApplication::translate("coefficients", "\320\232\321\201\320\265\320\275\320\276\320\275 (44+_129)", nullptr));
        cbxElements->setItemText(24, QCoreApplication::translate("coefficients", "\320\232\321\200\320\270\320\277\321\202\320\276\320\275 (28+_84)", nullptr));
        cbxElements->setItemText(25, QCoreApplication::translate("coefficients", "\320\232\321\200\320\270\320\277\321\202\320\276\320\275 (34+_84)", nullptr));
        cbxElements->setItemText(26, QCoreApplication::translate("coefficients", "\320\243\320\263\320\273\320\265\321\200\320\276\320\264 (4+_12)", nullptr));
        cbxElements->setItemText(27, QCoreApplication::translate("coefficients", "\320\226\320\265\320\273\320\265\320\267\320\276 (22+_56)", nullptr));
        cbxElements->setItemText(28, QCoreApplication::translate("coefficients", "\320\226\320\265\320\273\320\265\320\267\320\276 (14+_56)", nullptr));

        label_a_n_e->setText(QCoreApplication::translate("coefficients", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">a:</span></p></body></html>", nullptr));
        label_c_n_e->setText(QCoreApplication::translate("coefficients", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">c:</span></p></body></html>", nullptr));
        label_b_n_e->setText(QCoreApplication::translate("coefficients", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">b:</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class coefficients: public Ui_coefficients {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COEFFICIENTS_H
