#ifndef TIME_VOLTAGE_H
#define TIME_VOLTAGE_H

#include <QWidget>
#include <QDesktopServices>
#include <QUrl>
#include <QString>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "ui_time_voltage.h"

namespace Ui {
class time_voltage;
}

class time_voltage : public QWidget
{
    Q_OBJECT

public:
    explicit time_voltage(QWidget *parent = nullptr);
    ~time_voltage();

public slots:

    QString ui_read_from_file_t1_1();
    QString ui_read_from_file_t2_1();
    QString ui_read_from_file_t3_1();
    QString ui_read_from_file_t4_1();
    QString ui_read_from_file_t5_1();
    QString ui_read_from_file_t6_1();
    QString ui_read_from_file_t7_1();
    QString ui_read_from_file_t8_1();
    QString ui_read_from_file_t9_1();
    QString ui_read_from_file_t10_1();
    QString ui_read_from_file_t11_1();
    QString ui_read_from_file_t12_1();
    QString ui_read_from_file_t13_1();
    QString ui_read_from_file_t14_1();
    QString ui_read_from_file_t15_1();
    QString ui_read_from_file_u1_1();
    QString ui_read_from_file_u2_1();
    QString ui_read_from_file_u3_1();
    QString ui_read_from_file_u4_1();
    QString ui_read_from_file_u5_1();
    QString ui_read_from_file_u6_1();
    QString ui_read_from_file_u7_1();
    QString ui_read_from_file_u8_1();
    QString ui_read_from_file_u9_1();
    QString ui_read_from_file_u10_1();
    QString ui_read_from_file_u11_1();
    QString ui_read_from_file_u12_1();
    QString ui_read_from_file_u13_1();
    QString ui_read_from_file_u14_1();
    QString ui_read_from_file_u15_1();

    QString ui_read_from_file_t1_2();
    QString ui_read_from_file_t2_2();
    QString ui_read_from_file_t3_2();
    QString ui_read_from_file_t4_2();
    QString ui_read_from_file_t5_2();
    QString ui_read_from_file_t6_2();
    QString ui_read_from_file_t7_2();
    QString ui_read_from_file_t8_2();
    QString ui_read_from_file_t9_2();
    QString ui_read_from_file_t10_2();
    QString ui_read_from_file_t11_2();
    QString ui_read_from_file_t12_2();
    QString ui_read_from_file_t13_2();
    QString ui_read_from_file_t14_2();
    QString ui_read_from_file_t15_2();
    QString ui_read_from_file_u1_2();
    QString ui_read_from_file_u2_2();
    QString ui_read_from_file_u3_2();
    QString ui_read_from_file_u4_2();
    QString ui_read_from_file_u5_2();
    QString ui_read_from_file_u6_2();
    QString ui_read_from_file_u7_2();
    QString ui_read_from_file_u8_2();
    QString ui_read_from_file_u9_2();
    QString ui_read_from_file_u10_2();
    QString ui_read_from_file_u11_2();
    QString ui_read_from_file_u12_2();
    QString ui_read_from_file_u13_2();
    QString ui_read_from_file_u14_2();
    QString ui_read_from_file_u15_2();

    void ui_write_to_file_t1_1();
    void ui_write_to_file_t1_2();
    void ui_write_to_file_t1_3();
    void ui_write_to_file_t1_4();
    void ui_write_to_file_t1_5();
    void ui_write_to_file_t1_6();
    void ui_write_to_file_t1_7();
    void ui_write_to_file_t1_8();
    void ui_write_to_file_t1_9();
    void ui_write_to_file_t1_10();
    void ui_write_to_file_t1_11();
    void ui_write_to_file_t1_12();
    void ui_write_to_file_t1_13();
    void ui_write_to_file_t1_14();
    void ui_write_to_file_t1_15();
    void ui_write_to_file_u1_1();
    void ui_write_to_file_u1_2();
    void ui_write_to_file_u1_3();
    void ui_write_to_file_u1_4();
    void ui_write_to_file_u1_5();
    void ui_write_to_file_u1_6();
    void ui_write_to_file_u1_7();
    void ui_write_to_file_u1_8();
    void ui_write_to_file_u1_9();
    void ui_write_to_file_u1_10();
    void ui_write_to_file_u1_11();
    void ui_write_to_file_u1_12();
    void ui_write_to_file_u1_13();
    void ui_write_to_file_u1_14();
    void ui_write_to_file_u1_15();
    void ui_write_to_file_t2_1();
    void ui_write_to_file_t2_2();
    void ui_write_to_file_t2_3();
    void ui_write_to_file_t2_4();
    void ui_write_to_file_t2_5();
    void ui_write_to_file_t2_6();
    void ui_write_to_file_t2_7();
    void ui_write_to_file_t2_8();
    void ui_write_to_file_t2_9();
    void ui_write_to_file_t2_10();
    void ui_write_to_file_t2_11();
    void ui_write_to_file_t2_12();
    void ui_write_to_file_t2_13();
    void ui_write_to_file_t2_14();
    void ui_write_to_file_t2_15();
    void ui_write_to_file_u2_1();
    void ui_write_to_file_u2_2();
    void ui_write_to_file_u2_3();
    void ui_write_to_file_u2_4();
    void ui_write_to_file_u2_5();
    void ui_write_to_file_u2_6();
    void ui_write_to_file_u2_7();
    void ui_write_to_file_u2_8();
    void ui_write_to_file_u2_9();
    void ui_write_to_file_u2_10();
    void ui_write_to_file_u2_11();
    void ui_write_to_file_u2_12();
    void ui_write_to_file_u2_13();
    void ui_write_to_file_u2_14();
    void ui_write_to_file_u2_15();

    void ui_read_t1_1();
    void ui_read_t1_2to14();
    void ui_read_t1_15();
    void ui_read_u1_1();
    void ui_read_u1_2to14();
    void ui_read_u1_15();
    void ui_read_t2_1();
    void ui_read_t2_2to14();
    void ui_read_t2_15();
    void ui_read_u2_1();
    void ui_read_u2_2to14();
    void ui_read_u2_15();

private slots:

    void on_rewriteButton_clicked();

private:
    Ui::time_voltage *ui;
};

#endif // TIME_VOLTAGE_H
