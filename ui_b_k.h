/********************************************************************************
** Form generated from reading UI file 'b_k.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_B_K_H
#define UI_B_K_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_b_k
{
public:
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QPushButton *minusButtonK;
    QPushButton *writeButtonK;
    QPushButton *oneminusButtonK;
    QLabel *readed_value_K;
    QLabel *label_K;
    QPushButton *writeButtonB0;
    QPushButton *twominusButtonK;
    QPushButton *oneminusButtonB0;
    QLineEdit *writed_value_K;
    QPushButton *twoplusButtonB0;
    QPushButton *minusButtonB0;
    QPushButton *threeplusButtonK;
    QPushButton *oneplusButtonK;
    QPushButton *plusButtonB0;
    QLabel *label;
    QLabel *readed_value_B0;
    QPushButton *threeminusButtonK;
    QLabel *label_B0;
    QPushButton *oneplusButtonB0;
    QPushButton *plusButtonK;
    QLineEdit *writed_value_B0;
    QPushButton *threeplusButtonB0;
    QPushButton *twoplusButtonK;
    QPushButton *twominusButtonB0;
    QPushButton *threeminusButtonB0;

    void setupUi(QWidget *b_k)
    {
        if (b_k->objectName().isEmpty())
            b_k->setObjectName(QString::fromUtf8("b_k"));
        b_k->resize(973, 539);
        gridLayout = new QGridLayout(b_k);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(b_k);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 951, 517));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(10);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(5, 5, 5, 5);
        minusButtonK = new QPushButton(scrollAreaWidgetContents);
        minusButtonK->setObjectName(QString::fromUtf8("minusButtonK"));

        gridLayout_2->addWidget(minusButtonK, 8, 1, 1, 1);

        writeButtonK = new QPushButton(scrollAreaWidgetContents);
        writeButtonK->setObjectName(QString::fromUtf8("writeButtonK"));
        QFont font;
        font.setPointSize(18);
        writeButtonK->setFont(font);

        gridLayout_2->addWidget(writeButtonK, 5, 1, 1, 5);

        oneminusButtonK = new QPushButton(scrollAreaWidgetContents);
        oneminusButtonK->setObjectName(QString::fromUtf8("oneminusButtonK"));

        gridLayout_2->addWidget(oneminusButtonK, 8, 2, 1, 1);

        readed_value_K = new QLabel(scrollAreaWidgetContents);
        readed_value_K->setObjectName(QString::fromUtf8("readed_value_K"));
        readed_value_K->setFont(font);

        gridLayout_2->addWidget(readed_value_K, 7, 1, 1, 5);

        label_K = new QLabel(scrollAreaWidgetContents);
        label_K->setObjectName(QString::fromUtf8("label_K"));
        label_K->setFont(font);

        gridLayout_2->addWidget(label_K, 7, 0, 1, 1);

        writeButtonB0 = new QPushButton(scrollAreaWidgetContents);
        writeButtonB0->setObjectName(QString::fromUtf8("writeButtonB0"));
        writeButtonB0->setFont(font);

        gridLayout_2->addWidget(writeButtonB0, 1, 1, 1, 5);

        twominusButtonK = new QPushButton(scrollAreaWidgetContents);
        twominusButtonK->setObjectName(QString::fromUtf8("twominusButtonK"));

        gridLayout_2->addWidget(twominusButtonK, 8, 3, 1, 1);

        oneminusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        oneminusButtonB0->setObjectName(QString::fromUtf8("oneminusButtonB0"));

        gridLayout_2->addWidget(oneminusButtonB0, 4, 2, 1, 1);

        writed_value_K = new QLineEdit(scrollAreaWidgetContents);
        writed_value_K->setObjectName(QString::fromUtf8("writed_value_K"));
        writed_value_K->setFont(font);

        gridLayout_2->addWidget(writed_value_K, 5, 0, 1, 1);

        twoplusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        twoplusButtonB0->setObjectName(QString::fromUtf8("twoplusButtonB0"));

        gridLayout_2->addWidget(twoplusButtonB0, 2, 3, 1, 1);

        minusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        minusButtonB0->setObjectName(QString::fromUtf8("minusButtonB0"));

        gridLayout_2->addWidget(minusButtonB0, 4, 1, 1, 1);

        threeplusButtonK = new QPushButton(scrollAreaWidgetContents);
        threeplusButtonK->setObjectName(QString::fromUtf8("threeplusButtonK"));

        gridLayout_2->addWidget(threeplusButtonK, 6, 4, 1, 1);

        oneplusButtonK = new QPushButton(scrollAreaWidgetContents);
        oneplusButtonK->setObjectName(QString::fromUtf8("oneplusButtonK"));

        gridLayout_2->addWidget(oneplusButtonK, 6, 2, 1, 1);

        plusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        plusButtonB0->setObjectName(QString::fromUtf8("plusButtonB0"));

        gridLayout_2->addWidget(plusButtonB0, 2, 1, 1, 1);

        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        readed_value_B0 = new QLabel(scrollAreaWidgetContents);
        readed_value_B0->setObjectName(QString::fromUtf8("readed_value_B0"));
        readed_value_B0->setFont(font);

        gridLayout_2->addWidget(readed_value_B0, 3, 1, 1, 5);

        threeminusButtonK = new QPushButton(scrollAreaWidgetContents);
        threeminusButtonK->setObjectName(QString::fromUtf8("threeminusButtonK"));

        gridLayout_2->addWidget(threeminusButtonK, 8, 4, 1, 1);

        label_B0 = new QLabel(scrollAreaWidgetContents);
        label_B0->setObjectName(QString::fromUtf8("label_B0"));

        gridLayout_2->addWidget(label_B0, 3, 0, 1, 1);

        oneplusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        oneplusButtonB0->setObjectName(QString::fromUtf8("oneplusButtonB0"));

        gridLayout_2->addWidget(oneplusButtonB0, 2, 2, 1, 1);

        plusButtonK = new QPushButton(scrollAreaWidgetContents);
        plusButtonK->setObjectName(QString::fromUtf8("plusButtonK"));

        gridLayout_2->addWidget(plusButtonK, 6, 1, 1, 1);

        writed_value_B0 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_B0->setObjectName(QString::fromUtf8("writed_value_B0"));
        writed_value_B0->setFont(font);

        gridLayout_2->addWidget(writed_value_B0, 1, 0, 1, 1);

        threeplusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        threeplusButtonB0->setObjectName(QString::fromUtf8("threeplusButtonB0"));

        gridLayout_2->addWidget(threeplusButtonB0, 2, 4, 1, 1);

        twoplusButtonK = new QPushButton(scrollAreaWidgetContents);
        twoplusButtonK->setObjectName(QString::fromUtf8("twoplusButtonK"));

        gridLayout_2->addWidget(twoplusButtonK, 6, 3, 1, 1);

        twominusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        twominusButtonB0->setObjectName(QString::fromUtf8("twominusButtonB0"));

        gridLayout_2->addWidget(twominusButtonB0, 4, 3, 1, 1);

        threeminusButtonB0 = new QPushButton(scrollAreaWidgetContents);
        threeminusButtonB0->setObjectName(QString::fromUtf8("threeminusButtonB0"));

        gridLayout_2->addWidget(threeminusButtonB0, 4, 4, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);


        retranslateUi(b_k);

        QMetaObject::connectSlotsByName(b_k);
    } // setupUi

    void retranslateUi(QWidget *b_k)
    {
        b_k->setWindowTitle(QCoreApplication::translate("b_k", "Form", nullptr));
        minusButtonK->setText(QCoreApplication::translate("b_k", "-", nullptr));
        writeButtonK->setText(QCoreApplication::translate("b_k", "\320\227\320\260\320\264\320\260\321\202\321\214 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 K", nullptr));
        oneminusButtonK->setText(QCoreApplication::translate("b_k", "-", nullptr));
        readed_value_K->setText(QCoreApplication::translate("b_k", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\232</span></p></body></html>", nullptr));
        label_K->setText(QCoreApplication::translate("b_k", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\242\320\265\320\272\321\203\321\211\320\265\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\232:</span></p></body></html>", nullptr));
        writeButtonB0->setText(QCoreApplication::translate("b_k", "\320\227\320\260\320\264\320\260\321\202\321\214 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\2220", nullptr));
        twominusButtonK->setText(QCoreApplication::translate("b_k", "-", nullptr));
        oneminusButtonB0->setText(QCoreApplication::translate("b_k", "-", nullptr));
        twoplusButtonB0->setText(QCoreApplication::translate("b_k", "+", nullptr));
        minusButtonB0->setText(QCoreApplication::translate("b_k", "-", nullptr));
        threeplusButtonK->setText(QCoreApplication::translate("b_k", "+", nullptr));
        oneplusButtonK->setText(QCoreApplication::translate("b_k", "+", nullptr));
        plusButtonB0->setText(QCoreApplication::translate("b_k", "+", nullptr));
        label->setText(QCoreApplication::translate("b_k", "<html><head/><body><p>\320\230\321\201\320\277\320\276\320\273\321\214\320\267\321\203\320\271\321\202\320\265 \321\202\320\276\320\273\321\214\320\272\320\276 \321\206\320\270\321\204\321\200\321\213 \320\270 \321\202\320\276\321\207\320\272\321\203.</p></body></html>", nullptr));
        readed_value_B0->setText(QCoreApplication::translate("b_k", "<html><head/><body><p align=\"center\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\2220</p></body></html>", nullptr));
        threeminusButtonK->setText(QCoreApplication::translate("b_k", "-", nullptr));
        label_B0->setText(QCoreApplication::translate("b_k", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\242\320\265\320\272\321\203\321\211\320\265\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\2220:</span></p></body></html>", nullptr));
        oneplusButtonB0->setText(QCoreApplication::translate("b_k", "+", nullptr));
        plusButtonK->setText(QCoreApplication::translate("b_k", "+", nullptr));
        threeplusButtonB0->setText(QCoreApplication::translate("b_k", "+", nullptr));
        twoplusButtonK->setText(QCoreApplication::translate("b_k", "+", nullptr));
        twominusButtonB0->setText(QCoreApplication::translate("b_k", "-", nullptr));
        threeminusButtonB0->setText(QCoreApplication::translate("b_k", "-", nullptr));
    } // retranslateUi

};

namespace Ui {
    class b_k: public Ui_b_k {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_B_K_H
