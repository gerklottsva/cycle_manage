#include "coefficients.h"

coefficients::coefficients(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::coefficients)
{
    ui->setupUi(this);

    setWindowTitle("Коэффициенты коррекции");

    connect(ui->coefButton, SIGNAL(clicked()), this, SLOT(ui_coef_change()));
}

coefficients::~coefficients()
{
    delete ui;
}

void coefficients::ui_coef_change()
{
    if(ui->cbxElements->currentText()=="Протоны (1+_1)"){
    ui->label_a_n_e->setText("a: 5957.953613");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 17616459.12");
    }
    if(ui->cbxElements->currentText()=="Дейтоны (1+_2)"){
    ui->label_a_n_e->setText("a: 2981.273256");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4410907.532");
    }
    if(ui->cbxElements->currentText()=="Гелий (2+_3)"){
    ui->label_a_n_e->setText("a: 3983.237877");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 7874026.486");
    }
    if(ui->cbxElements->currentText()=="Гелий (2+_4)"){
    ui->label_a_n_e->setText("a: 3001.165297");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4469965.974");
    }
    if(ui->cbxElements->currentText()=="Гелий (1+_4)"){
    ui->label_a_n_e->setText("a: 1500.582649");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 1117491.493");
    }
    if(ui->cbxElements->currentText()=="Сера (16+_32)"){
    ui->label_a_n_e->setText("a: 3005.718054");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4483538.106");
    }
    if(ui->cbxElements->currentText()=="Кремний (14+_28)"){
    ui->label_a_n_e->setText("a: 3030.955716");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4559146.708");
    }
    if(ui->cbxElements->currentText()=="Магний (12+_24)"){
    ui->label_a_n_e->setText("a: 3004.991898");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4481371.996");
    }
    if(ui->cbxElements->currentText()=="Кислород (8+_16)"){
    ui->label_a_n_e->setText("a: 3004.073715");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4478633.826");
    }
    if(ui->cbxElements->currentText()=="Углерод (6+_12)"){
    ui->label_a_n_e->setText("a: 3003.119762");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4475789.87");
    }
    if(ui->cbxElements->currentText()=="Литий (3+_6)"){
    ui->label_a_n_e->setText("a: 3002.142211");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4472876.5");
    }
    if(ui->cbxElements->currentText()=="Хлор (17+_35)"){
    ui->label_a_n_e->setText("a: 2919.915076");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4231212.394");
    }
    if(ui->cbxElements->currentText()=="Магний 12+_25"){
    ui->label_a_n_e->setText("a: 2884.600724");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4129484.13");
    }
    if(ui->cbxElements->currentText()=="Фтор (9+_19)"){
    ui->label_a_n_e->setText("a: 2845.256282");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 4017604.362");
    }
    if(ui->cbxElements->currentText()=="Магний (12+_26)"){
    ui->label_a_n_e->setText("a: 2773.909426");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 3818641.939");
    }
    if(ui->cbxElements->currentText()=="Неон (10+_22)"){
    ui->label_a_n_e->setText("a: 2731.108979");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 3701710.454");
    }
    if(ui->cbxElements->currentText()=="Аргон (18+_40)"){
    ui->label_a_n_e->setText("a: 2705.275413");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 3632012.682");
}
    if(ui->cbxElements->currentText()=="Криптон (36+_84)"){
    ui->label_a_n_e->setText("a: 2575.952972");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 3293064.561");
}
    if(ui->cbxElements->currentText()=="Литий (3+_7)"){
    ui->label_a_n_e->setText("a: 2568.127271");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 3273086.406");
}
    if(ui->cbxElements->currentText()=="Аргон (16+_40)"){
    ui->label_a_n_e->setText("a: 2403.778956");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2867566.136");
}
    if(ui->cbxElements->currentText()=="Криптон (30+_84)"){
    ui->label_a_n_e->setText("a: 2146.091449");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2285708.448");
}
    if(ui->cbxElements->currentText()=="Аргон (14+_40)"){
    ui->label_a_n_e->setText("a: 2103.127955");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2195107.418");
}
    if(ui->cbxElements->currentText()=="Криптон (29+_84)"){
    ui->label_a_n_e->setText("a: 2074.752236");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2136273.572");
}
    if(ui->cbxElements->currentText()=="Ксенон (44+_129)"){
    ui->label_a_n_e->setText("a: 2049.796693");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2085191.575");
}
    if(ui->cbxElements->currentText()=="Криптон (28+_84)"){
    ui->label_a_n_e->setText("a: 2003.182423");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 1991431.567");
}
    if(ui->cbxElements->currentText()=="Криптон (34+_84)"){
    ui->label_a_n_e->setText("a: 2432.365147");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2936174.948");
}
    if(ui->cbxElements->currentText()=="Углерод (4+_12)"){
    ui->label_a_n_e->setText("a: 2001.713726");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 1988512.473");
}
    if(ui->cbxElements->currentText()=="Железо (22+_56)"){
    ui->label_a_n_e->setText("a: 2360.91105");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 2766200.37");
}
    if(ui->cbxElements->currentText()=="Железо (14+_56)"){
    ui->label_a_n_e->setText("a: 1502.279188");
    ui->label_b_n_e->setText("b: 867339");
    ui->label_c_n_e->setText("c: 1120019.765");
}
}
