#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <start_cycle.h>
#include <coefficients.h>
#include <time_voltage.h>
#include <b_k.h>
#include <graph.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:

private slots:

    void on_startStopCycleButton_clicked();

    void on_coefficientsButton_clicked();

    void on_timeVoltageButton_clicked();

    void on_b_kButton_clicked();

    void on_graphButton_clicked();

private:
    Ui::MainWindow *ui;
    start_cycle start_cycle;
    coefficients coefficients;
    time_voltage time_voltage;
    b_k b_k;
    graph graph;
};
#endif // MAINWINDOW_H
