﻿#Запуск контроллера в цикле
#Фатькин Г. А. 27.12.2020,

import telnetlib

HOST = "192.168.1.99"
tn = telnetlib.Telnet(HOST,timeout=20)
print("Connected")

tn.read_until(b">",10)
tn.write('mode 1'.encode() + b'\r' b"\n" )

B0 = "0.0546"
K = "0.893"

print("Set start magnetic field " + B0)
tn.read_until(b">",10)
string = "edit 1 "+B0
print(string)
tn.write(string.encode() + b'\r' b"\n")

print("Set Koeff " + K)
tn.read_until(b">",10)
string = "edit 73 "+K
print(string)
tn.write(string.encode() + b'\r' b"\n")

maxdelay = 7
delay = 0.5
maxtries = maxdelay/delay

while 1:
	print("Ready")   
	print("Sending fast cycle")
	tn.write('fast'.encode() + b'\n' +b'\r')
	print("Cycle started")
	for i in range(int(maxtries)): 
		a = tn.read_until(b"0",delay)
		if a is b'0':
			break
		if i%5 == 1:
			print(i)
	if i == maxtries:
		print("Cycle not ended")
		break
	
tn.close()


#if __name__ == '__main__':
#    print("Генерируем файл для тестера: {0}".format(tester_filename))
#    write_Tester(tester_filename)
#    print("Генерируем файлы для контроллера")
#    write_Controller(HV1Stage1_filename,U_HV1Stage1[1])
#    write_Controller(HV2Stage1_filename,U_HV2Stage1[1])
#    write_Controller(HV1Stage2_filename,U_HV1Stage2[1])
#    write_Controller(HV2Stage2_filename,U_HV2Stage2[1])
#    print("Готово!")
#    drawCombined()