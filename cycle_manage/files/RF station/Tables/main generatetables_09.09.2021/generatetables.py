﻿#Генерация файлов таблиц для модуля-Тестера и модуля-контроллера бустера НИКА
#Фатькин Г. А. 27.11.2014, Дубна
#Ильин И.В. 10.12.2015, Новосибирск

#Имена файлов для записи таблиц
tester_filename = "tester_table.txt"
HV1Stage1_filename = "controller_HV1Stage1.txt"
HV2Stage1_filename = "controller_HV2Stage1.txt"
HV1Stage2_filename = "controller_HV1Stage2.txt"
HV2Stage2_filename = "controller_HV2Stage2.txt"

#Синхроимпульсы (В секундах)
Start_Cycle = 0       #Старт цикла
Reserv_1 = 0.09       #Резерв 1 (Запуск Куркина)
Bref_OK = 0.89         #Bref
Start_1HV = 0.998     #Старт I
Start_Cooling = 1.880 #Охлаждение
Start_2HV = 1.881     #Старт II
End_Acel = 1.882      #Конец ускорения
End_Cycle = 3.0    #Конец цикла

End_Table = 3.0     #Конец таблицы тестера, должен быть > End_Cycle!

#Массив на	пряжений на тестере (1 В соответствует dФ/dt = 1.2 Т/сек ):
U_tester_points = [
[0, 0.2, 0.201        , 1.3          ,1.301, 1.980, 1.981                , 4.75                 , 4.751, 5.752], #Время,с, закончить в End_Table!
[0, 0  , 0.0734/1.1/1.2, 0.0734/1.1/1.2,0    , 0    , (0.356-0.0734)/2.75/1.2, (0.356-0.0734)/2.75/1.2, 0    , 0   ]] #U

#Массив напряжений ВЧ для станции 1 в первом этапе (время отсчитывается от Старт I)
U_HV1Stage1_points = [
[0.0,0.2,0.25,0.46,0.51,1.0,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1],    #Время, с
[-0.1,  -0.1  ,0.7,0.7,5.0,5.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]     #U, kV
#Массив напряжений ВЧ для станции 2 в первом этапе (время отсчитывается от Старт I)
U_HV2Stage1_points = U_HV1Stage1_points #Сейчас напряжения ВЧ - одинаковые

#Массив напряжений ВЧ для станции 1 во втором этапе (время отсчитывается от Старт II)
U_HV1Stage2_points = [
[0.0   , 0.1,0.75,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5],     #Время, с
[0.0,4.0,4.0,4.0,0.0 ,0.0 ,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]     #U, kV
#Массив напряжений ВЧ для станции 2 во втором этапе (время отсчитывается от Старт II)
U_HV2Stage2_points = U_HV1Stage2_points

deltaT_tester = 5e-6 # Отсчёт тестера: 5 мкс
deltaT_controller = 40e-6 # Отсчёт контроллера: 40 мкс

#------------------------------------------------------------------------------------
# Конец конфигурации
#------------------------------------------------------------------------------------

import matplotlib.pyplot as plt
from math import ceil
import numpy as np


#Линейная экстраполяция по точкам points с шагом step
#На входе массив неэкстраполированных точек [[x1,x2,...],[y1,y2,...]], отсортированный по x,
#не должен содержать точек с одинаковыми x.
#Возвращает массив экстраполированных точек [[x1,x1+step,...][y1,y1',....]]
def interpolate_linear(points,step):
    result = [[],[]]
    for i,point in enumerate(points[0][0:-1]):
        for x in np.arange(points[0][i],points[0][i+1],step):
            result[0].append(x)
            result[1].append(points[1][i] + (points[1][i+1]-points[1][i])/(points[0][i+1]-points[0][i])*(x-points[0][i]))
    result[0].append(points[0][-1])
    result[1].append(points[1][-1])
    return result

# Преобразование Напряжение-код
def calcDACCode(U):
    code = int((1-U)*(2**15))
    return code;

#Генерация таблиц:
U_tester = interpolate_linear(U_tester_points,deltaT_tester)
U_HV1Stage1 = interpolate_linear(U_HV1Stage1_points,deltaT_controller)
U_HV2Stage1 = interpolate_linear(U_HV2Stage1_points,deltaT_controller)
U_HV1Stage2 = interpolate_linear(U_HV1Stage2_points,deltaT_controller)
U_HV2Stage2 = interpolate_linear(U_HV2Stage2_points,deltaT_controller)


# Отрисовка таблиц на отдельных графиках
def drawTables():
    plt.figure(1)
    plt.subplot(311)
    plt.plot(U_tester[0],U_tester[1])
    plt.ylabel('U_tester [V]')
    plt.grid(True)
    plt.subplot(312)
    plt.plot(U_HV1Stage1[0],U_HV1Stage1[1])
    plt.plot(U_HV2Stage1[0],U_HV2Stage1[1])
    plt.ylabel('U_HV Stage1 [kV]')
    plt.legend(['Station1','Station2'])
    plt.grid(True)
    plt.subplot(313)
    plt.plot(U_HV1Stage2[0],U_HV1Stage2[1])
    plt.plot(U_HV2Stage2[0],U_HV2Stage2[1])
    plt.ylabel('U_HV Stage2 [kV]')
    plt.legend(['Station1','Station2'])
    plt.grid(True)
    plt.show()    

# Отрисовка комбинированного графика
def drawCombined():
    SynchPulses = [Start_Cycle,Reserv_1,Bref_OK,Start_1HV,Start_Cooling,Start_2HV,End_Acel,End_Cycle]
    SynchPulsesNames = ["1","2","3","4","5","6","7","8"]
    plt.figure(2)
    plt.subplot(211)
    plt.plot(U_tester[0],U_tester[1])
    plt.ylabel('U_tester [V]')
    for i,x in enumerate(SynchPulses):
        plt.axvline(x,color='r')
        plt.annotate(SynchPulsesNames[i],(x,1),color='r')
    plt.subplot(212)
    plt.plot(np.array(U_HV1Stage1[0])+np.ones(len(U_HV1Stage1[0]))*Bref_OK,U_HV1Stage1[1])
    plt.plot(np.array(U_HV1Stage2[0])+np.ones(len(U_HV1Stage2[0]))*Start_2HV,U_HV1Stage2[1])
    plt.plot(np.array(U_HV2Stage1[0])+np.ones(len(U_HV2Stage1[0]))*Bref_OK,U_HV1Stage1[1])
    plt.plot(np.array(U_HV2Stage2[0])+np.ones(len(U_HV2Stage2[0]))*Start_2HV,U_HV1Stage2[1])
    plt.ylabel('U_HV [kV]')
    plt.legend(['1-StageI','1-StageII','2-StageI','2-StageII'])
    for i,x in enumerate(SynchPulses):
        plt.axvline(x,color='r')
        plt.annotate(SynchPulsesNames[i],(x,1),color='r')
    plt.show()

#Запись таблицы для Тестера
def write_Tester(filename):
    dt = deltaT_tester
    with open(filename,"w",newline='\n') as f:
        f.write("muxswt 1\n")
        #Записываем таблицу
        f.write("settbl {0} {1}\n".format(len(U_tester[0]),0))
        for u in U_tester[1]:
            f.write("{0}\n".format(calcDACCode(u)))
        #Задаём синхроимпульсы
        f.write("setsync 255\n")
        f.write("{0}\n".format(ceil(Start_Cycle/dt)))
        f.write("{0}\n".format(ceil(Bref_OK/dt)))
        f.write("{0}\n".format(ceil(Start_1HV/dt)))
        f.write("{0}\n".format(ceil(Start_Cooling/dt)))
        f.write("{0}\n".format(ceil(Start_2HV/dt)))
        f.write("{0}\n".format(ceil(End_Acel/dt)))
        f.write("{0}\n".format(ceil(Reserv_1/dt)))
        f.write("{0}\n".format(ceil(End_Cycle/dt)))

def write_Controller(filename,table):
    with open(filename,"w") as f:
        for u in table:
            f.write("{0:.6}\n".format(u))

if __name__ == '__main__':
    print("Генерируем файл для тестера: {0}".format(tester_filename))
    write_Tester(tester_filename)
    print("Генерируем файлы для контроллера")
    write_Controller(HV1Stage1_filename,U_HV1Stage1[1])
    write_Controller(HV2Stage1_filename,U_HV2Stage1[1])
    write_Controller(HV1Stage2_filename,U_HV1Stage2[1])
    write_Controller(HV2Stage2_filename,U_HV2Stage2[1])
    print("Готово!")
    drawCombined()