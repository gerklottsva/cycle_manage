#include "graph.h"

graph::graph(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::graph)
{
    ui->setupUi(this);

    graphPainter();
}

void graph::graphPainter()
{
    setWindowTitle("График");

    ui->widget->clearGraphs();

    ui->widget->setLocale(QLocale(QLocale::Russian, QLocale::RussianFederation));
    ui->widget->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(9);
    ui->widget->legend->setFont(legendFont);
    ui->widget->legend->setBrush(QBrush(QColor(255,255,255,230)));
    ui->widget->axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignBottom|Qt::AlignRight);

    ui->widget->addGraph();
    ui->widget->graph(0)->setPen(QPen(Qt::blue));
    ui->widget->graph(0)->setBrush(QBrush(QColor(0,0,255,20)));
    ui->widget->graph(0)->setName("HV 1");
    ui->widget->addGraph();
    ui->widget->graph(1)->setPen(QPen(Qt::red));
    ui->widget->graph(1)->setBrush(QBrush(QColor(255,0,0,20)));
    ui->widget->graph(1)->setName("HV 2");

    QVector<double> x1(15);
    QVector<double> y1(15);
    QVector<double> x2(15);
    QVector<double> y2(15);

    time_voltage t;

    x1[0] = t.time_voltage::ui_read_from_file_t1_1().toDouble();
    x1[1] = t.time_voltage::ui_read_from_file_t2_1().toDouble();
    x1[2] = t.time_voltage::ui_read_from_file_t3_1().toDouble();
    x1[3] = t.time_voltage::ui_read_from_file_t4_1().toDouble();
    x1[4] = t.time_voltage::ui_read_from_file_t5_1().toDouble();
    x1[5] = t.time_voltage::ui_read_from_file_t6_1().toDouble();
    x1[6] = t.time_voltage::ui_read_from_file_t7_1().toDouble();
    x1[7] = t.time_voltage::ui_read_from_file_t8_1().toDouble();
    x1[8] = t.time_voltage::ui_read_from_file_t9_1().toDouble();
    x1[9] = t.time_voltage::ui_read_from_file_t10_1().toDouble();
    x1[10] = t.time_voltage::ui_read_from_file_t11_1().toDouble();
    x1[11] = t.time_voltage::ui_read_from_file_t12_1().toDouble();
    x1[12] = t.time_voltage::ui_read_from_file_t13_1().toDouble();
    x1[13] = t.time_voltage::ui_read_from_file_t14_1().toDouble();
    x1[14] = t.time_voltage::ui_read_from_file_t15_1().toDouble();
    y1[0] = t.time_voltage::ui_read_from_file_u1_1().toDouble();
    y1[1] = t.time_voltage::ui_read_from_file_u2_1().toDouble();
    y1[2] = t.time_voltage::ui_read_from_file_u3_1().toDouble();
    y1[3] = t.time_voltage::ui_read_from_file_u4_1().toDouble();
    y1[4] = t.time_voltage::ui_read_from_file_u5_1().toDouble();
    y1[5] = t.time_voltage::ui_read_from_file_u6_1().toDouble();
    y1[6] = t.time_voltage::ui_read_from_file_u7_1().toDouble();
    y1[7] = t.time_voltage::ui_read_from_file_u8_1().toDouble();
    y1[8] = t.time_voltage::ui_read_from_file_u9_1().toDouble();
    y1[9] = t.time_voltage::ui_read_from_file_u10_1().toDouble();
    y1[10] = t.time_voltage::ui_read_from_file_u11_1().toDouble();
    y1[11] = t.time_voltage::ui_read_from_file_u12_1().toDouble();
    y1[12] = t.time_voltage::ui_read_from_file_u13_1().toDouble();
    y1[13] = t.time_voltage::ui_read_from_file_u14_1().toDouble();
    y1[14] = t.time_voltage::ui_read_from_file_u15_1().toDouble();

    x2[0] = t.time_voltage::ui_read_from_file_t1_2().toDouble();
    x2[1] = t.time_voltage::ui_read_from_file_t2_2().toDouble();
    x2[2] = t.time_voltage::ui_read_from_file_t3_2().toDouble();
    x2[3] = t.time_voltage::ui_read_from_file_t4_2().toDouble();
    x2[4] = t.time_voltage::ui_read_from_file_t5_2().toDouble();
    x2[5] = t.time_voltage::ui_read_from_file_t6_2().toDouble();
    x2[6] = t.time_voltage::ui_read_from_file_t7_2().toDouble();
    x2[7] = t.time_voltage::ui_read_from_file_t8_2().toDouble();
    x2[8] = t.time_voltage::ui_read_from_file_t9_2().toDouble();
    x2[9] = t.time_voltage::ui_read_from_file_t10_2().toDouble();
    x2[10] = t.time_voltage::ui_read_from_file_t11_2().toDouble();
    x2[11] = t.time_voltage::ui_read_from_file_t12_2().toDouble();
    x2[12] = t.time_voltage::ui_read_from_file_t13_2().toDouble();
    x2[13] = t.time_voltage::ui_read_from_file_t14_2().toDouble();
    x2[14] = t.time_voltage::ui_read_from_file_t15_2().toDouble();
    y2[0] = t.time_voltage::ui_read_from_file_u1_2().toDouble();
    y2[1] = t.time_voltage::ui_read_from_file_u2_2().toDouble();
    y2[2] = t.time_voltage::ui_read_from_file_u3_2().toDouble();
    y2[3] = t.time_voltage::ui_read_from_file_u4_2().toDouble();
    y2[4] = t.time_voltage::ui_read_from_file_u5_2().toDouble();
    y2[5] = t.time_voltage::ui_read_from_file_u6_2().toDouble();
    y2[6] = t.time_voltage::ui_read_from_file_u7_2().toDouble();
    y2[7] = t.time_voltage::ui_read_from_file_u8_2().toDouble();
    y2[8] = t.time_voltage::ui_read_from_file_u9_2().toDouble();
    y2[9] = t.time_voltage::ui_read_from_file_u10_2().toDouble();
    y2[10] = t.time_voltage::ui_read_from_file_u11_2().toDouble();
    y2[11] = t.time_voltage::ui_read_from_file_u12_2().toDouble();
    y2[12] = t.time_voltage::ui_read_from_file_u13_2().toDouble();
    y2[13] = t.time_voltage::ui_read_from_file_u14_2().toDouble();
    y2[14] = t.time_voltage::ui_read_from_file_u15_2().toDouble();

    ui->widget->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));
    ui->widget->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));

    ui->widget->xAxis->setLabel("t,sec");
    ui->widget->yAxis->setLabel("U,kV");
    ui->widget->xAxis2->setVisible(true);
    ui->widget->xAxis2->setTickLabels(false);
    ui->widget->yAxis2->setVisible(true);
    ui->widget->yAxis2->setTickLabels(false);

    connect(ui->widget->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->widget->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->widget->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->widget->yAxis2, SLOT(setRange(QCPRange)));

    ui->widget->graph(0)->setData(x1,y1);
    ui->widget->graph(1)->setData(x2,y2);

    ui->widget->graph(0)->rescaleAxes();

    ui->widget->graph(1)->rescaleAxes(true);

    ui->widget->setInteraction(QCP::iRangeDrag);
    ui->widget->setInteraction(QCP::iRangeZoom);
    ui->widget->setInteraction(QCP::iSelectPlottables);

    ui->widget->replot();
}

graph::~graph()
{
    delete ui;
}


void graph::on_updateGraphButton_clicked()
{
    graphPainter();
}
