#ifndef COEFFICIENTS_H
#define COEFFICIENTS_H

#include <QWidget>
#include "ui_coefficients.h"

namespace Ui {
class coefficients;
}

class coefficients : public QWidget
{
    Q_OBJECT

public:
    explicit coefficients(QWidget *parent = nullptr);
    ~coefficients();

public slots:
    void ui_coef_change();

private:
    Ui::coefficients *ui;
};

#endif // COEFFICIENTS_H
