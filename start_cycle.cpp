#include "start_cycle.h"

start_cycle::start_cycle(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::start_cycle)
{
    ui->setupUi(this);

    setWindowTitle("Управление запуском цикла");

    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(start_file()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(stop_file()));
}

start_cycle::~start_cycle()
{
    delete ui;
}

void start_cycle::start_file()
{
    QDesktopServices::openUrl(QUrl("file:D:\\///cycle.py", QUrl::TolerantMode));
}

void start_cycle::stop_file()
{
    QDesktopServices::openUrl(QUrl("file:D:\\///killcyclepy", QUrl::TolerantMode));
}
