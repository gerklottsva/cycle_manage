#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>
#include <QVector>
#include "ui_graph.h"
#include "time_voltage.h"
#include "ui_time_voltage.h"
#include <QString>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

namespace Ui {
class graph;
}

class graph : public QWidget
{
    Q_OBJECT

public:
    explicit graph(QWidget *parent = nullptr);
    ~graph();

public slots:
void graphPainter();

private slots:
    void on_updateGraphButton_clicked();

private:
    Ui::graph *ui;
};

#endif // GRAPH_H
