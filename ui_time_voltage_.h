/********************************************************************************
** Form generated from reading UI file 'time_voltage_.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIME_VOLTAGE__H
#define UI_TIME_VOLTAGE__H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_time_voltage
{
public:
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QLineEdit *writed_value_t2_15;
    QPushButton *writeButtont2_12;
    QLabel *readed_value_t2;
    QLineEdit *writed_value_t1_2;
    QLabel *label_u1_2;
    QLineEdit *writed_value_u2_10;
    QLineEdit *writed_value_t1_11;
    QLineEdit *writed_value_u1_8;
    QLabel *label_t2_7;
    QLineEdit *writed_value_u1_1;
    QPushButton *writeButtonu2_8;
    QLabel *label_t1_2;
    QLabel *label_u2_1;
    QLabel *label_u1_7;
    QLabel *label_t2_3;
    QLabel *readed_value_t1;
    QPushButton *writeButtont1_9;
    QLineEdit *writed_value_t1_9;
    QLineEdit *writed_value_t2_13;
    QLabel *label_t2_6;
    QLineEdit *writed_value_u2_13;
    QLineEdit *writed_value_u2_12;
    QLineEdit *writed_value_t2_5;
    QLabel *label_t2_14;
    QLineEdit *writed_value_u1_9;
    QPushButton *writeButtont1_1;
    QLabel *label_t2_5;
    QPushButton *writeButtont2_14;
    QLabel *label_u2_8;
    QLabel *label_t2_12;
    QPushButton *writeButtont1_10;
    QPushButton *writeButtont2_15;
    QPushButton *writeButtont2_1;
    QLineEdit *writed_value_t2_9;
    QLineEdit *writed_value_t1_10;
    QPushButton *writeButtonu1_14;
    QLabel *label_t2_1;
    QLabel *label_t1_15;
    QPushButton *writeButtont1_6;
    QPushButton *writeButtonu2_14;
    QPushButton *writeButtonu1_13;
    QLabel *label_U2;
    QLabel *label_t1_12;
    QLabel *label_u1_5;
    QLineEdit *writed_value_t2_12;
    QLineEdit *writed_value_u1_14;
    QPushButton *writeButtonu1_9;
    QLabel *label_t1_14;
    QPushButton *writeButtont2_9;
    QPushButton *writeButtont1_11;
    QPushButton *writeButtonu2_13;
    QLineEdit *writed_value_u1_2;
    QPushButton *writeButtont1_14;
    QLabel *label_t1_3;
    QLineEdit *writed_value_u1_7;
    QPushButton *writeButtont2_6;
    QLineEdit *writed_value_t2_8;
    QPushButton *writeButtonu1_12;
    QLineEdit *writed_value_t2_2;
    QLineEdit *writed_value_t1_5;
    QLineEdit *writed_value_u1_13;
    QPushButton *writeButtont2_8;
    QLineEdit *writed_value_u1_6;
    QLabel *label_t1_7;
    QLabel *label_u2_5;
    QLineEdit *writed_value_u2_11;
    QLabel *label_u1_1;
    QPushButton *writeButtonu2_5;
    QLabel *label_u1_4;
    QLabel *label_t1_4;
    QLineEdit *writed_value_t1_1;
    QLabel *label_u2_14;
    QPushButton *writeButtont1_15;
    QLabel *label_u2_13;
    QLineEdit *writed_value_u1_11;
    QLabel *label_u2_4;
    QLabel *label_u2_2;
    QLabel *label_t1_13;
    QLabel *label_u1_12;
    QLabel *label_u1_14;
    QLineEdit *writed_value_u2_7;
    QLabel *label_t1_8;
    QPushButton *writeButtonu2_2;
    QLineEdit *writed_value_u1_3;
    QPushButton *writeButtonu1_1;
    QLineEdit *writed_value_u2_6;
    QPushButton *writeButtonu2_11;
    QLineEdit *writed_value_t2_3;
    QPushButton *writeButtonu1_8;
    QPushButton *writeButtonu2_7;
    QPushButton *writeButtont2_10;
    QPushButton *writeButtonu1_7;
    QPushButton *writeButtonu2_12;
    QLineEdit *writed_value_u2_9;
    QLabel *label_t2_11;
    QLineEdit *writed_value_t1_12;
    QLabel *label_t2_2;
    QPushButton *writeButtont2_2;
    QLabel *label_u2_9;
    QLineEdit *writed_value_t2_11;
    QLineEdit *writed_value_t2_10;
    QLabel *label_u1_3;
    QLineEdit *writed_value_t1_14;
    QLabel *label_u1_10;
    QPushButton *writeButtont1_7;
    QPushButton *writeButtonu1_2;
    QPushButton *writeButtonu2_1;
    QLabel *label_u1_9;
    QLabel *label_u2_3;
    QLabel *label_t2_13;
    QLabel *label_u2_15;
    QLabel *label_t2_15;
    QPushButton *writeButtont1_13;
    QPushButton *writeButtont1_8;
    QPushButton *writeButtonu1_11;
    QLineEdit *writed_value_t2_7;
    QLabel *label_t1_1;
    QLineEdit *writed_value_t1_15;
    QPushButton *writeButtont2_3;
    QLabel *label_t1_11;
    QPushButton *writeButtonu1_5;
    QPushButton *writeButtonu2_6;
    QLabel *label_t1;
    QLabel *label_u2_11;
    QLineEdit *writed_value_t2_6;
    QPushButton *writeButtont1_4;
    QLabel *label_t1_5;
    QLineEdit *writed_value_t1_3;
    QPushButton *writeButtont1_3;
    QLineEdit *writed_value_t1_4;
    QLineEdit *writed_value_t1_8;
    QLabel *label_u2_10;
    QLineEdit *writed_value_u2_1;
    QLineEdit *writed_value_t1_6;
    QLabel *label_t2_8;
    QPushButton *writeButtonu2_3;
    QLabel *label_t1_6;
    QLineEdit *writed_value_u2_4;
    QLineEdit *writed_value_t2_1;
    QPushButton *writeButtonu1_15;
    QLineEdit *writed_value_u2_2;
    QLabel *label_u2_6;
    QPushButton *writeButtont1_2;
    QLineEdit *writed_value_u2_8;
    QLineEdit *writed_value_u1_4;
    QPushButton *writeButtonu1_4;
    QLabel *label;
    QLineEdit *writed_value_t2_14;
    QPushButton *writeButtonu2_15;
    QLineEdit *writed_value_u2_3;
    QPushButton *writeButtonu2_9;
    QLabel *label_U1;
    QPushButton *writeButtont2_5;
    QPushButton *writeButtonu2_10;
    QPushButton *writeButtont2_13;
    QLabel *label_t1_10;
    QLabel *label_t1_9;
    QLabel *label_t2_9;
    QPushButton *writeButtont1_5;
    QLabel *readed_value_U2;
    QLabel *label_u2_7;
    QLabel *label_u1_13;
    QPushButton *writeButtonu1_10;
    QPushButton *writeButtont2_4;
    QLineEdit *writed_value_u1_10;
    QLineEdit *writed_value_u2_15;
    QLineEdit *writed_value_t1_7;
    QLineEdit *writed_value_t2_4;
    QPushButton *writeButtont1_12;
    QLabel *label_t2;
    QLabel *label_t2_4;
    QLabel *label_t2_10;
    QLabel *label_u2_12;
    QLineEdit *writed_value_u1_12;
    QLineEdit *writed_value_t1_13;
    QLabel *label_u1_11;
    QPushButton *writeButtont2_7;
    QLineEdit *writed_value_u1_5;
    QPushButton *writeButtonu1_3;
    QLabel *label_u1_8;
    QLineEdit *writed_value_u1_15;
    QPushButton *writeButtonu1_6;
    QLabel *label_u1_6;
    QPushButton *writeButtonu2_4;
    QLabel *readed_value_U1;
    QLabel *label_u1_15;
    QPushButton *writeButtont2_11;
    QLineEdit *writed_value_u2_14;
    QLineEdit *writed_value_u2_5;
    QPushButton *rewriteButton;

    void setupUi(QWidget *time_voltage)
    {
        if (time_voltage->objectName().isEmpty())
            time_voltage->setObjectName(QString::fromUtf8("time_voltage"));
        time_voltage->resize(1522, 736);
        gridLayout = new QGridLayout(time_voltage);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        scrollArea = new QScrollArea(time_voltage);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1774, 700));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(10);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(5, 5, 5, 5);
        writed_value_t2_15 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_15->setObjectName(QString::fromUtf8("writed_value_t2_15"));
        QFont font;
        font.setPointSize(18);
        writed_value_t2_15->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_15, 9, 16, 1, 1);

        writeButtont2_12 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_12->setObjectName(QString::fromUtf8("writeButtont2_12"));
        writeButtont2_12->setFont(font);

        gridLayout_2->addWidget(writeButtont2_12, 10, 13, 1, 1);

        readed_value_t2 = new QLabel(scrollAreaWidgetContents);
        readed_value_t2->setObjectName(QString::fromUtf8("readed_value_t2"));
        readed_value_t2->setFont(font);

        gridLayout_2->addWidget(readed_value_t2, 12, 2, 1, 7);

        writed_value_t1_2 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_2->setObjectName(QString::fromUtf8("writed_value_t1_2"));
        writed_value_t1_2->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_2, 1, 3, 1, 1);

        label_u1_2 = new QLabel(scrollAreaWidgetContents);
        label_u1_2->setObjectName(QString::fromUtf8("label_u1_2"));
        label_u1_2->setFont(font);

        gridLayout_2->addWidget(label_u1_2, 7, 3, 1, 1);

        writed_value_u2_10 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_10->setObjectName(QString::fromUtf8("writed_value_u2_10"));
        writed_value_u2_10->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_10, 13, 11, 1, 1);

        writed_value_t1_11 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_11->setObjectName(QString::fromUtf8("writed_value_t1_11"));
        writed_value_t1_11->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_11, 1, 12, 1, 1);

        writed_value_u1_8 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_8->setObjectName(QString::fromUtf8("writed_value_u1_8"));
        writed_value_u1_8->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_8, 5, 9, 1, 1);

        label_t2_7 = new QLabel(scrollAreaWidgetContents);
        label_t2_7->setObjectName(QString::fromUtf8("label_t2_7"));
        label_t2_7->setFont(font);

        gridLayout_2->addWidget(label_t2_7, 11, 8, 1, 1);

        writed_value_u1_1 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_1->setObjectName(QString::fromUtf8("writed_value_u1_1"));
        writed_value_u1_1->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_1, 5, 2, 1, 1);

        writeButtonu2_8 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_8->setObjectName(QString::fromUtf8("writeButtonu2_8"));
        writeButtonu2_8->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_8, 14, 9, 1, 1);

        label_t1_2 = new QLabel(scrollAreaWidgetContents);
        label_t1_2->setObjectName(QString::fromUtf8("label_t1_2"));
        label_t1_2->setFont(font);

        gridLayout_2->addWidget(label_t1_2, 3, 3, 1, 1);

        label_u2_1 = new QLabel(scrollAreaWidgetContents);
        label_u2_1->setObjectName(QString::fromUtf8("label_u2_1"));
        label_u2_1->setFont(font);

        gridLayout_2->addWidget(label_u2_1, 15, 2, 1, 1);

        label_u1_7 = new QLabel(scrollAreaWidgetContents);
        label_u1_7->setObjectName(QString::fromUtf8("label_u1_7"));
        label_u1_7->setFont(font);

        gridLayout_2->addWidget(label_u1_7, 7, 8, 1, 1);

        label_t2_3 = new QLabel(scrollAreaWidgetContents);
        label_t2_3->setObjectName(QString::fromUtf8("label_t2_3"));
        label_t2_3->setFont(font);

        gridLayout_2->addWidget(label_t2_3, 11, 4, 1, 1);

        readed_value_t1 = new QLabel(scrollAreaWidgetContents);
        readed_value_t1->setObjectName(QString::fromUtf8("readed_value_t1"));
        readed_value_t1->setFont(font);

        gridLayout_2->addWidget(readed_value_t1, 4, 2, 1, 7);

        writeButtont1_9 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_9->setObjectName(QString::fromUtf8("writeButtont1_9"));
        writeButtont1_9->setFont(font);

        gridLayout_2->addWidget(writeButtont1_9, 2, 10, 1, 1);

        writed_value_t1_9 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_9->setObjectName(QString::fromUtf8("writed_value_t1_9"));
        writed_value_t1_9->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_9, 1, 10, 1, 1);

        writed_value_t2_13 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_13->setObjectName(QString::fromUtf8("writed_value_t2_13"));
        writed_value_t2_13->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_13, 9, 14, 1, 1);

        label_t2_6 = new QLabel(scrollAreaWidgetContents);
        label_t2_6->setObjectName(QString::fromUtf8("label_t2_6"));
        label_t2_6->setFont(font);

        gridLayout_2->addWidget(label_t2_6, 11, 7, 1, 1);

        writed_value_u2_13 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_13->setObjectName(QString::fromUtf8("writed_value_u2_13"));
        writed_value_u2_13->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_13, 13, 14, 1, 1);

        writed_value_u2_12 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_12->setObjectName(QString::fromUtf8("writed_value_u2_12"));
        writed_value_u2_12->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_12, 13, 13, 1, 1);

        writed_value_t2_5 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_5->setObjectName(QString::fromUtf8("writed_value_t2_5"));
        writed_value_t2_5->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_5, 9, 6, 1, 1);

        label_t2_14 = new QLabel(scrollAreaWidgetContents);
        label_t2_14->setObjectName(QString::fromUtf8("label_t2_14"));
        label_t2_14->setFont(font);

        gridLayout_2->addWidget(label_t2_14, 11, 15, 1, 1);

        writed_value_u1_9 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_9->setObjectName(QString::fromUtf8("writed_value_u1_9"));
        writed_value_u1_9->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_9, 5, 10, 1, 1);

        writeButtont1_1 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_1->setObjectName(QString::fromUtf8("writeButtont1_1"));
        writeButtont1_1->setFont(font);

        gridLayout_2->addWidget(writeButtont1_1, 2, 2, 1, 1);

        label_t2_5 = new QLabel(scrollAreaWidgetContents);
        label_t2_5->setObjectName(QString::fromUtf8("label_t2_5"));
        label_t2_5->setFont(font);

        gridLayout_2->addWidget(label_t2_5, 11, 6, 1, 1);

        writeButtont2_14 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_14->setObjectName(QString::fromUtf8("writeButtont2_14"));
        writeButtont2_14->setFont(font);

        gridLayout_2->addWidget(writeButtont2_14, 10, 15, 1, 1);

        label_u2_8 = new QLabel(scrollAreaWidgetContents);
        label_u2_8->setObjectName(QString::fromUtf8("label_u2_8"));
        label_u2_8->setFont(font);

        gridLayout_2->addWidget(label_u2_8, 15, 9, 1, 1);

        label_t2_12 = new QLabel(scrollAreaWidgetContents);
        label_t2_12->setObjectName(QString::fromUtf8("label_t2_12"));
        label_t2_12->setFont(font);

        gridLayout_2->addWidget(label_t2_12, 11, 13, 1, 1);

        writeButtont1_10 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_10->setObjectName(QString::fromUtf8("writeButtont1_10"));
        writeButtont1_10->setFont(font);

        gridLayout_2->addWidget(writeButtont1_10, 2, 11, 1, 1);

        writeButtont2_15 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_15->setObjectName(QString::fromUtf8("writeButtont2_15"));
        writeButtont2_15->setFont(font);

        gridLayout_2->addWidget(writeButtont2_15, 10, 16, 1, 1);

        writeButtont2_1 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_1->setObjectName(QString::fromUtf8("writeButtont2_1"));
        writeButtont2_1->setFont(font);

        gridLayout_2->addWidget(writeButtont2_1, 10, 2, 1, 1);

        writed_value_t2_9 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_9->setObjectName(QString::fromUtf8("writed_value_t2_9"));
        writed_value_t2_9->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_9, 9, 10, 1, 1);

        writed_value_t1_10 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_10->setObjectName(QString::fromUtf8("writed_value_t1_10"));
        writed_value_t1_10->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_10, 1, 11, 1, 1);

        writeButtonu1_14 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_14->setObjectName(QString::fromUtf8("writeButtonu1_14"));
        writeButtonu1_14->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_14, 6, 15, 1, 1);

        label_t2_1 = new QLabel(scrollAreaWidgetContents);
        label_t2_1->setObjectName(QString::fromUtf8("label_t2_1"));
        label_t2_1->setFont(font);

        gridLayout_2->addWidget(label_t2_1, 11, 2, 1, 1);

        label_t1_15 = new QLabel(scrollAreaWidgetContents);
        label_t1_15->setObjectName(QString::fromUtf8("label_t1_15"));
        label_t1_15->setFont(font);

        gridLayout_2->addWidget(label_t1_15, 3, 16, 1, 1);

        writeButtont1_6 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_6->setObjectName(QString::fromUtf8("writeButtont1_6"));
        writeButtont1_6->setFont(font);

        gridLayout_2->addWidget(writeButtont1_6, 2, 7, 1, 1);

        writeButtonu2_14 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_14->setObjectName(QString::fromUtf8("writeButtonu2_14"));
        writeButtonu2_14->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_14, 14, 15, 1, 1);

        writeButtonu1_13 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_13->setObjectName(QString::fromUtf8("writeButtonu1_13"));
        writeButtonu1_13->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_13, 6, 14, 1, 1);

        label_U2 = new QLabel(scrollAreaWidgetContents);
        label_U2->setObjectName(QString::fromUtf8("label_U2"));

        gridLayout_2->addWidget(label_U2, 16, 0, 1, 2);

        label_t1_12 = new QLabel(scrollAreaWidgetContents);
        label_t1_12->setObjectName(QString::fromUtf8("label_t1_12"));
        label_t1_12->setFont(font);

        gridLayout_2->addWidget(label_t1_12, 3, 13, 1, 1);

        label_u1_5 = new QLabel(scrollAreaWidgetContents);
        label_u1_5->setObjectName(QString::fromUtf8("label_u1_5"));
        label_u1_5->setFont(font);

        gridLayout_2->addWidget(label_u1_5, 7, 6, 1, 1);

        writed_value_t2_12 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_12->setObjectName(QString::fromUtf8("writed_value_t2_12"));
        writed_value_t2_12->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_12, 9, 13, 1, 1);

        writed_value_u1_14 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_14->setObjectName(QString::fromUtf8("writed_value_u1_14"));
        writed_value_u1_14->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_14, 5, 15, 1, 1);

        writeButtonu1_9 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_9->setObjectName(QString::fromUtf8("writeButtonu1_9"));
        writeButtonu1_9->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_9, 6, 10, 1, 1);

        label_t1_14 = new QLabel(scrollAreaWidgetContents);
        label_t1_14->setObjectName(QString::fromUtf8("label_t1_14"));
        label_t1_14->setFont(font);

        gridLayout_2->addWidget(label_t1_14, 3, 15, 1, 1);

        writeButtont2_9 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_9->setObjectName(QString::fromUtf8("writeButtont2_9"));
        writeButtont2_9->setFont(font);

        gridLayout_2->addWidget(writeButtont2_9, 10, 10, 1, 1);

        writeButtont1_11 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_11->setObjectName(QString::fromUtf8("writeButtont1_11"));
        writeButtont1_11->setFont(font);

        gridLayout_2->addWidget(writeButtont1_11, 2, 12, 1, 1);

        writeButtonu2_13 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_13->setObjectName(QString::fromUtf8("writeButtonu2_13"));
        writeButtonu2_13->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_13, 14, 14, 1, 1);

        writed_value_u1_2 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_2->setObjectName(QString::fromUtf8("writed_value_u1_2"));
        writed_value_u1_2->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_2, 5, 3, 1, 1);

        writeButtont1_14 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_14->setObjectName(QString::fromUtf8("writeButtont1_14"));
        writeButtont1_14->setFont(font);

        gridLayout_2->addWidget(writeButtont1_14, 2, 15, 1, 1);

        label_t1_3 = new QLabel(scrollAreaWidgetContents);
        label_t1_3->setObjectName(QString::fromUtf8("label_t1_3"));
        label_t1_3->setFont(font);

        gridLayout_2->addWidget(label_t1_3, 3, 4, 1, 1);

        writed_value_u1_7 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_7->setObjectName(QString::fromUtf8("writed_value_u1_7"));
        writed_value_u1_7->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_7, 5, 8, 1, 1);

        writeButtont2_6 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_6->setObjectName(QString::fromUtf8("writeButtont2_6"));
        writeButtont2_6->setFont(font);

        gridLayout_2->addWidget(writeButtont2_6, 10, 7, 1, 1);

        writed_value_t2_8 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_8->setObjectName(QString::fromUtf8("writed_value_t2_8"));
        writed_value_t2_8->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_8, 9, 9, 1, 1);

        writeButtonu1_12 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_12->setObjectName(QString::fromUtf8("writeButtonu1_12"));
        writeButtonu1_12->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_12, 6, 13, 1, 1);

        writed_value_t2_2 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_2->setObjectName(QString::fromUtf8("writed_value_t2_2"));
        writed_value_t2_2->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_2, 9, 3, 1, 1);

        writed_value_t1_5 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_5->setObjectName(QString::fromUtf8("writed_value_t1_5"));
        writed_value_t1_5->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_5, 1, 6, 1, 1);

        writed_value_u1_13 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_13->setObjectName(QString::fromUtf8("writed_value_u1_13"));
        writed_value_u1_13->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_13, 5, 14, 1, 1);

        writeButtont2_8 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_8->setObjectName(QString::fromUtf8("writeButtont2_8"));
        writeButtont2_8->setFont(font);

        gridLayout_2->addWidget(writeButtont2_8, 10, 9, 1, 1);

        writed_value_u1_6 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_6->setObjectName(QString::fromUtf8("writed_value_u1_6"));
        writed_value_u1_6->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_6, 5, 7, 1, 1);

        label_t1_7 = new QLabel(scrollAreaWidgetContents);
        label_t1_7->setObjectName(QString::fromUtf8("label_t1_7"));
        label_t1_7->setFont(font);

        gridLayout_2->addWidget(label_t1_7, 3, 8, 1, 1);

        label_u2_5 = new QLabel(scrollAreaWidgetContents);
        label_u2_5->setObjectName(QString::fromUtf8("label_u2_5"));
        label_u2_5->setFont(font);

        gridLayout_2->addWidget(label_u2_5, 15, 6, 1, 1);

        writed_value_u2_11 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_11->setObjectName(QString::fromUtf8("writed_value_u2_11"));
        writed_value_u2_11->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_11, 13, 12, 1, 1);

        label_u1_1 = new QLabel(scrollAreaWidgetContents);
        label_u1_1->setObjectName(QString::fromUtf8("label_u1_1"));
        label_u1_1->setFont(font);

        gridLayout_2->addWidget(label_u1_1, 7, 2, 1, 1);

        writeButtonu2_5 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_5->setObjectName(QString::fromUtf8("writeButtonu2_5"));
        writeButtonu2_5->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_5, 14, 6, 1, 1);

        label_u1_4 = new QLabel(scrollAreaWidgetContents);
        label_u1_4->setObjectName(QString::fromUtf8("label_u1_4"));
        label_u1_4->setFont(font);

        gridLayout_2->addWidget(label_u1_4, 7, 5, 1, 1);

        label_t1_4 = new QLabel(scrollAreaWidgetContents);
        label_t1_4->setObjectName(QString::fromUtf8("label_t1_4"));
        label_t1_4->setFont(font);

        gridLayout_2->addWidget(label_t1_4, 3, 5, 1, 1);

        writed_value_t1_1 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_1->setObjectName(QString::fromUtf8("writed_value_t1_1"));
        writed_value_t1_1->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_1, 1, 2, 1, 1);

        label_u2_14 = new QLabel(scrollAreaWidgetContents);
        label_u2_14->setObjectName(QString::fromUtf8("label_u2_14"));
        label_u2_14->setFont(font);

        gridLayout_2->addWidget(label_u2_14, 15, 15, 1, 1);

        writeButtont1_15 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_15->setObjectName(QString::fromUtf8("writeButtont1_15"));
        writeButtont1_15->setFont(font);

        gridLayout_2->addWidget(writeButtont1_15, 2, 16, 1, 1);

        label_u2_13 = new QLabel(scrollAreaWidgetContents);
        label_u2_13->setObjectName(QString::fromUtf8("label_u2_13"));
        label_u2_13->setFont(font);

        gridLayout_2->addWidget(label_u2_13, 15, 14, 1, 1);

        writed_value_u1_11 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_11->setObjectName(QString::fromUtf8("writed_value_u1_11"));
        writed_value_u1_11->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_11, 5, 12, 1, 1);

        label_u2_4 = new QLabel(scrollAreaWidgetContents);
        label_u2_4->setObjectName(QString::fromUtf8("label_u2_4"));
        label_u2_4->setFont(font);

        gridLayout_2->addWidget(label_u2_4, 15, 5, 1, 1);

        label_u2_2 = new QLabel(scrollAreaWidgetContents);
        label_u2_2->setObjectName(QString::fromUtf8("label_u2_2"));
        label_u2_2->setFont(font);

        gridLayout_2->addWidget(label_u2_2, 15, 3, 1, 1);

        label_t1_13 = new QLabel(scrollAreaWidgetContents);
        label_t1_13->setObjectName(QString::fromUtf8("label_t1_13"));
        label_t1_13->setFont(font);

        gridLayout_2->addWidget(label_t1_13, 3, 14, 1, 1);

        label_u1_12 = new QLabel(scrollAreaWidgetContents);
        label_u1_12->setObjectName(QString::fromUtf8("label_u1_12"));
        label_u1_12->setFont(font);

        gridLayout_2->addWidget(label_u1_12, 7, 13, 1, 1);

        label_u1_14 = new QLabel(scrollAreaWidgetContents);
        label_u1_14->setObjectName(QString::fromUtf8("label_u1_14"));
        label_u1_14->setFont(font);

        gridLayout_2->addWidget(label_u1_14, 7, 15, 1, 1);

        writed_value_u2_7 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_7->setObjectName(QString::fromUtf8("writed_value_u2_7"));
        writed_value_u2_7->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_7, 13, 8, 1, 1);

        label_t1_8 = new QLabel(scrollAreaWidgetContents);
        label_t1_8->setObjectName(QString::fromUtf8("label_t1_8"));
        label_t1_8->setFont(font);

        gridLayout_2->addWidget(label_t1_8, 3, 9, 1, 1);

        writeButtonu2_2 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_2->setObjectName(QString::fromUtf8("writeButtonu2_2"));
        writeButtonu2_2->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_2, 14, 3, 1, 1);

        writed_value_u1_3 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_3->setObjectName(QString::fromUtf8("writed_value_u1_3"));
        writed_value_u1_3->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_3, 5, 4, 1, 1);

        writeButtonu1_1 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_1->setObjectName(QString::fromUtf8("writeButtonu1_1"));
        writeButtonu1_1->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_1, 6, 2, 1, 1);

        writed_value_u2_6 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_6->setObjectName(QString::fromUtf8("writed_value_u2_6"));
        writed_value_u2_6->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_6, 13, 7, 1, 1);

        writeButtonu2_11 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_11->setObjectName(QString::fromUtf8("writeButtonu2_11"));
        writeButtonu2_11->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_11, 14, 12, 1, 1);

        writed_value_t2_3 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_3->setObjectName(QString::fromUtf8("writed_value_t2_3"));
        writed_value_t2_3->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_3, 9, 4, 1, 1);

        writeButtonu1_8 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_8->setObjectName(QString::fromUtf8("writeButtonu1_8"));
        writeButtonu1_8->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_8, 6, 9, 1, 1);

        writeButtonu2_7 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_7->setObjectName(QString::fromUtf8("writeButtonu2_7"));
        writeButtonu2_7->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_7, 14, 8, 1, 1);

        writeButtont2_10 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_10->setObjectName(QString::fromUtf8("writeButtont2_10"));
        writeButtont2_10->setFont(font);

        gridLayout_2->addWidget(writeButtont2_10, 10, 11, 1, 1);

        writeButtonu1_7 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_7->setObjectName(QString::fromUtf8("writeButtonu1_7"));
        writeButtonu1_7->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_7, 6, 8, 1, 1);

        writeButtonu2_12 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_12->setObjectName(QString::fromUtf8("writeButtonu2_12"));
        writeButtonu2_12->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_12, 14, 13, 1, 1);

        writed_value_u2_9 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_9->setObjectName(QString::fromUtf8("writed_value_u2_9"));
        writed_value_u2_9->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_9, 13, 10, 1, 1);

        label_t2_11 = new QLabel(scrollAreaWidgetContents);
        label_t2_11->setObjectName(QString::fromUtf8("label_t2_11"));
        label_t2_11->setFont(font);

        gridLayout_2->addWidget(label_t2_11, 11, 12, 1, 1);

        writed_value_t1_12 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_12->setObjectName(QString::fromUtf8("writed_value_t1_12"));
        writed_value_t1_12->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_12, 1, 13, 1, 1);

        label_t2_2 = new QLabel(scrollAreaWidgetContents);
        label_t2_2->setObjectName(QString::fromUtf8("label_t2_2"));
        label_t2_2->setFont(font);

        gridLayout_2->addWidget(label_t2_2, 11, 3, 1, 1);

        writeButtont2_2 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_2->setObjectName(QString::fromUtf8("writeButtont2_2"));
        writeButtont2_2->setFont(font);

        gridLayout_2->addWidget(writeButtont2_2, 10, 3, 1, 1);

        label_u2_9 = new QLabel(scrollAreaWidgetContents);
        label_u2_9->setObjectName(QString::fromUtf8("label_u2_9"));
        label_u2_9->setFont(font);

        gridLayout_2->addWidget(label_u2_9, 15, 10, 1, 1);

        writed_value_t2_11 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_11->setObjectName(QString::fromUtf8("writed_value_t2_11"));
        writed_value_t2_11->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_11, 9, 12, 1, 1);

        writed_value_t2_10 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_10->setObjectName(QString::fromUtf8("writed_value_t2_10"));
        writed_value_t2_10->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_10, 9, 11, 1, 1);

        label_u1_3 = new QLabel(scrollAreaWidgetContents);
        label_u1_3->setObjectName(QString::fromUtf8("label_u1_3"));
        label_u1_3->setFont(font);

        gridLayout_2->addWidget(label_u1_3, 7, 4, 1, 1);

        writed_value_t1_14 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_14->setObjectName(QString::fromUtf8("writed_value_t1_14"));
        writed_value_t1_14->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_14, 1, 15, 1, 1);

        label_u1_10 = new QLabel(scrollAreaWidgetContents);
        label_u1_10->setObjectName(QString::fromUtf8("label_u1_10"));
        label_u1_10->setFont(font);

        gridLayout_2->addWidget(label_u1_10, 7, 11, 1, 1);

        writeButtont1_7 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_7->setObjectName(QString::fromUtf8("writeButtont1_7"));
        writeButtont1_7->setFont(font);

        gridLayout_2->addWidget(writeButtont1_7, 2, 8, 1, 1);

        writeButtonu1_2 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_2->setObjectName(QString::fromUtf8("writeButtonu1_2"));
        writeButtonu1_2->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_2, 6, 3, 1, 1);

        writeButtonu2_1 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_1->setObjectName(QString::fromUtf8("writeButtonu2_1"));
        writeButtonu2_1->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_1, 14, 2, 1, 1);

        label_u1_9 = new QLabel(scrollAreaWidgetContents);
        label_u1_9->setObjectName(QString::fromUtf8("label_u1_9"));
        label_u1_9->setFont(font);

        gridLayout_2->addWidget(label_u1_9, 7, 10, 1, 1);

        label_u2_3 = new QLabel(scrollAreaWidgetContents);
        label_u2_3->setObjectName(QString::fromUtf8("label_u2_3"));
        label_u2_3->setFont(font);

        gridLayout_2->addWidget(label_u2_3, 15, 4, 1, 1);

        label_t2_13 = new QLabel(scrollAreaWidgetContents);
        label_t2_13->setObjectName(QString::fromUtf8("label_t2_13"));
        label_t2_13->setFont(font);

        gridLayout_2->addWidget(label_t2_13, 11, 14, 1, 1);

        label_u2_15 = new QLabel(scrollAreaWidgetContents);
        label_u2_15->setObjectName(QString::fromUtf8("label_u2_15"));
        label_u2_15->setFont(font);

        gridLayout_2->addWidget(label_u2_15, 15, 16, 1, 1);

        label_t2_15 = new QLabel(scrollAreaWidgetContents);
        label_t2_15->setObjectName(QString::fromUtf8("label_t2_15"));
        label_t2_15->setFont(font);

        gridLayout_2->addWidget(label_t2_15, 11, 16, 1, 1);

        writeButtont1_13 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_13->setObjectName(QString::fromUtf8("writeButtont1_13"));
        writeButtont1_13->setFont(font);

        gridLayout_2->addWidget(writeButtont1_13, 2, 14, 1, 1);

        writeButtont1_8 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_8->setObjectName(QString::fromUtf8("writeButtont1_8"));
        writeButtont1_8->setFont(font);

        gridLayout_2->addWidget(writeButtont1_8, 2, 9, 1, 1);

        writeButtonu1_11 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_11->setObjectName(QString::fromUtf8("writeButtonu1_11"));
        writeButtonu1_11->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_11, 6, 12, 1, 1);

        writed_value_t2_7 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_7->setObjectName(QString::fromUtf8("writed_value_t2_7"));
        writed_value_t2_7->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_7, 9, 8, 1, 1);

        label_t1_1 = new QLabel(scrollAreaWidgetContents);
        label_t1_1->setObjectName(QString::fromUtf8("label_t1_1"));
        QFont font1;
        font1.setPointSize(18);
        font1.setKerning(true);
        label_t1_1->setFont(font1);
        label_t1_1->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(label_t1_1, 3, 2, 1, 1);

        writed_value_t1_15 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_15->setObjectName(QString::fromUtf8("writed_value_t1_15"));
        writed_value_t1_15->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_15, 1, 16, 1, 1);

        writeButtont2_3 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_3->setObjectName(QString::fromUtf8("writeButtont2_3"));
        writeButtont2_3->setFont(font);

        gridLayout_2->addWidget(writeButtont2_3, 10, 4, 1, 1);

        label_t1_11 = new QLabel(scrollAreaWidgetContents);
        label_t1_11->setObjectName(QString::fromUtf8("label_t1_11"));
        label_t1_11->setFont(font);

        gridLayout_2->addWidget(label_t1_11, 3, 12, 1, 1);

        writeButtonu1_5 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_5->setObjectName(QString::fromUtf8("writeButtonu1_5"));
        writeButtonu1_5->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_5, 6, 6, 1, 1);

        writeButtonu2_6 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_6->setObjectName(QString::fromUtf8("writeButtonu2_6"));
        writeButtonu2_6->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_6, 14, 7, 1, 1);

        label_t1 = new QLabel(scrollAreaWidgetContents);
        label_t1->setObjectName(QString::fromUtf8("label_t1"));

        gridLayout_2->addWidget(label_t1, 4, 0, 1, 2);

        label_u2_11 = new QLabel(scrollAreaWidgetContents);
        label_u2_11->setObjectName(QString::fromUtf8("label_u2_11"));
        label_u2_11->setFont(font);

        gridLayout_2->addWidget(label_u2_11, 15, 12, 1, 1);

        writed_value_t2_6 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_6->setObjectName(QString::fromUtf8("writed_value_t2_6"));
        writed_value_t2_6->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_6, 9, 7, 1, 1);

        writeButtont1_4 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_4->setObjectName(QString::fromUtf8("writeButtont1_4"));
        writeButtont1_4->setFont(font);

        gridLayout_2->addWidget(writeButtont1_4, 2, 5, 1, 1);

        label_t1_5 = new QLabel(scrollAreaWidgetContents);
        label_t1_5->setObjectName(QString::fromUtf8("label_t1_5"));
        label_t1_5->setFont(font);

        gridLayout_2->addWidget(label_t1_5, 3, 6, 1, 1);

        writed_value_t1_3 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_3->setObjectName(QString::fromUtf8("writed_value_t1_3"));
        writed_value_t1_3->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_3, 1, 4, 1, 1);

        writeButtont1_3 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_3->setObjectName(QString::fromUtf8("writeButtont1_3"));
        writeButtont1_3->setFont(font);

        gridLayout_2->addWidget(writeButtont1_3, 2, 4, 1, 1);

        writed_value_t1_4 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_4->setObjectName(QString::fromUtf8("writed_value_t1_4"));
        writed_value_t1_4->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_4, 1, 5, 1, 1);

        writed_value_t1_8 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_8->setObjectName(QString::fromUtf8("writed_value_t1_8"));
        writed_value_t1_8->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_8, 1, 9, 1, 1);

        label_u2_10 = new QLabel(scrollAreaWidgetContents);
        label_u2_10->setObjectName(QString::fromUtf8("label_u2_10"));
        label_u2_10->setFont(font);

        gridLayout_2->addWidget(label_u2_10, 15, 11, 1, 1);

        writed_value_u2_1 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_1->setObjectName(QString::fromUtf8("writed_value_u2_1"));
        writed_value_u2_1->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_1, 13, 2, 1, 1);

        writed_value_t1_6 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_6->setObjectName(QString::fromUtf8("writed_value_t1_6"));
        writed_value_t1_6->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_6, 1, 7, 1, 1);

        label_t2_8 = new QLabel(scrollAreaWidgetContents);
        label_t2_8->setObjectName(QString::fromUtf8("label_t2_8"));
        label_t2_8->setFont(font);

        gridLayout_2->addWidget(label_t2_8, 11, 9, 1, 1);

        writeButtonu2_3 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_3->setObjectName(QString::fromUtf8("writeButtonu2_3"));
        writeButtonu2_3->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_3, 14, 4, 1, 1);

        label_t1_6 = new QLabel(scrollAreaWidgetContents);
        label_t1_6->setObjectName(QString::fromUtf8("label_t1_6"));
        label_t1_6->setFont(font);

        gridLayout_2->addWidget(label_t1_6, 3, 7, 1, 1);

        writed_value_u2_4 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_4->setObjectName(QString::fromUtf8("writed_value_u2_4"));
        writed_value_u2_4->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_4, 13, 5, 1, 1);

        writed_value_t2_1 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_1->setObjectName(QString::fromUtf8("writed_value_t2_1"));
        writed_value_t2_1->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_1, 9, 2, 1, 1);

        writeButtonu1_15 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_15->setObjectName(QString::fromUtf8("writeButtonu1_15"));
        writeButtonu1_15->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_15, 6, 16, 1, 1);

        writed_value_u2_2 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_2->setObjectName(QString::fromUtf8("writed_value_u2_2"));
        writed_value_u2_2->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_2, 13, 3, 1, 1);

        label_u2_6 = new QLabel(scrollAreaWidgetContents);
        label_u2_6->setObjectName(QString::fromUtf8("label_u2_6"));
        label_u2_6->setFont(font);

        gridLayout_2->addWidget(label_u2_6, 15, 7, 1, 1);

        writeButtont1_2 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_2->setObjectName(QString::fromUtf8("writeButtont1_2"));
        writeButtont1_2->setFont(font);

        gridLayout_2->addWidget(writeButtont1_2, 2, 3, 1, 1);

        writed_value_u2_8 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_8->setObjectName(QString::fromUtf8("writed_value_u2_8"));
        writed_value_u2_8->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_8, 13, 9, 1, 1);

        writed_value_u1_4 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_4->setObjectName(QString::fromUtf8("writed_value_u1_4"));
        writed_value_u1_4->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_4, 5, 5, 1, 1);

        writeButtonu1_4 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_4->setObjectName(QString::fromUtf8("writeButtonu1_4"));
        writeButtonu1_4->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_4, 6, 5, 1, 1);

        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout_2->addWidget(label, 1, 0, 1, 1);

        writed_value_t2_14 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_14->setObjectName(QString::fromUtf8("writed_value_t2_14"));
        writed_value_t2_14->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_14, 9, 15, 1, 1);

        writeButtonu2_15 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_15->setObjectName(QString::fromUtf8("writeButtonu2_15"));
        writeButtonu2_15->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_15, 14, 16, 1, 1);

        writed_value_u2_3 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_3->setObjectName(QString::fromUtf8("writed_value_u2_3"));
        writed_value_u2_3->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_3, 13, 4, 1, 1);

        writeButtonu2_9 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_9->setObjectName(QString::fromUtf8("writeButtonu2_9"));
        writeButtonu2_9->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_9, 14, 10, 1, 1);

        label_U1 = new QLabel(scrollAreaWidgetContents);
        label_U1->setObjectName(QString::fromUtf8("label_U1"));

        gridLayout_2->addWidget(label_U1, 8, 0, 1, 1);

        writeButtont2_5 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_5->setObjectName(QString::fromUtf8("writeButtont2_5"));
        writeButtont2_5->setFont(font);

        gridLayout_2->addWidget(writeButtont2_5, 10, 6, 1, 1);

        writeButtonu2_10 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_10->setObjectName(QString::fromUtf8("writeButtonu2_10"));
        writeButtonu2_10->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_10, 14, 11, 1, 1);

        writeButtont2_13 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_13->setObjectName(QString::fromUtf8("writeButtont2_13"));
        writeButtont2_13->setFont(font);

        gridLayout_2->addWidget(writeButtont2_13, 10, 14, 1, 1);

        label_t1_10 = new QLabel(scrollAreaWidgetContents);
        label_t1_10->setObjectName(QString::fromUtf8("label_t1_10"));
        label_t1_10->setFont(font);

        gridLayout_2->addWidget(label_t1_10, 3, 11, 1, 1);

        label_t1_9 = new QLabel(scrollAreaWidgetContents);
        label_t1_9->setObjectName(QString::fromUtf8("label_t1_9"));
        label_t1_9->setFont(font);

        gridLayout_2->addWidget(label_t1_9, 3, 10, 1, 1);

        label_t2_9 = new QLabel(scrollAreaWidgetContents);
        label_t2_9->setObjectName(QString::fromUtf8("label_t2_9"));
        label_t2_9->setFont(font);

        gridLayout_2->addWidget(label_t2_9, 11, 10, 1, 1);

        writeButtont1_5 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_5->setObjectName(QString::fromUtf8("writeButtont1_5"));
        writeButtont1_5->setFont(font);

        gridLayout_2->addWidget(writeButtont1_5, 2, 6, 1, 1);

        readed_value_U2 = new QLabel(scrollAreaWidgetContents);
        readed_value_U2->setObjectName(QString::fromUtf8("readed_value_U2"));
        readed_value_U2->setFont(font);

        gridLayout_2->addWidget(readed_value_U2, 16, 2, 1, 7);

        label_u2_7 = new QLabel(scrollAreaWidgetContents);
        label_u2_7->setObjectName(QString::fromUtf8("label_u2_7"));
        label_u2_7->setFont(font);

        gridLayout_2->addWidget(label_u2_7, 15, 8, 1, 1);

        label_u1_13 = new QLabel(scrollAreaWidgetContents);
        label_u1_13->setObjectName(QString::fromUtf8("label_u1_13"));
        label_u1_13->setFont(font);

        gridLayout_2->addWidget(label_u1_13, 7, 14, 1, 1);

        writeButtonu1_10 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_10->setObjectName(QString::fromUtf8("writeButtonu1_10"));
        writeButtonu1_10->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_10, 6, 11, 1, 1);

        writeButtont2_4 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_4->setObjectName(QString::fromUtf8("writeButtont2_4"));
        writeButtont2_4->setFont(font);

        gridLayout_2->addWidget(writeButtont2_4, 10, 5, 1, 1);

        writed_value_u1_10 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_10->setObjectName(QString::fromUtf8("writed_value_u1_10"));
        writed_value_u1_10->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_10, 5, 11, 1, 1);

        writed_value_u2_15 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_15->setObjectName(QString::fromUtf8("writed_value_u2_15"));
        writed_value_u2_15->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_15, 13, 16, 1, 1);

        writed_value_t1_7 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_7->setObjectName(QString::fromUtf8("writed_value_t1_7"));
        writed_value_t1_7->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_7, 1, 8, 1, 1);

        writed_value_t2_4 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t2_4->setObjectName(QString::fromUtf8("writed_value_t2_4"));
        writed_value_t2_4->setFont(font);

        gridLayout_2->addWidget(writed_value_t2_4, 9, 5, 1, 1);

        writeButtont1_12 = new QPushButton(scrollAreaWidgetContents);
        writeButtont1_12->setObjectName(QString::fromUtf8("writeButtont1_12"));
        writeButtont1_12->setFont(font);

        gridLayout_2->addWidget(writeButtont1_12, 2, 13, 1, 1);

        label_t2 = new QLabel(scrollAreaWidgetContents);
        label_t2->setObjectName(QString::fromUtf8("label_t2"));

        gridLayout_2->addWidget(label_t2, 12, 0, 1, 2);

        label_t2_4 = new QLabel(scrollAreaWidgetContents);
        label_t2_4->setObjectName(QString::fromUtf8("label_t2_4"));
        label_t2_4->setFont(font);

        gridLayout_2->addWidget(label_t2_4, 11, 5, 1, 1);

        label_t2_10 = new QLabel(scrollAreaWidgetContents);
        label_t2_10->setObjectName(QString::fromUtf8("label_t2_10"));
        label_t2_10->setFont(font);

        gridLayout_2->addWidget(label_t2_10, 11, 11, 1, 1);

        label_u2_12 = new QLabel(scrollAreaWidgetContents);
        label_u2_12->setObjectName(QString::fromUtf8("label_u2_12"));
        label_u2_12->setFont(font);

        gridLayout_2->addWidget(label_u2_12, 15, 13, 1, 1);

        writed_value_u1_12 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_12->setObjectName(QString::fromUtf8("writed_value_u1_12"));
        writed_value_u1_12->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_12, 5, 13, 1, 1);

        writed_value_t1_13 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_t1_13->setObjectName(QString::fromUtf8("writed_value_t1_13"));
        writed_value_t1_13->setFont(font);

        gridLayout_2->addWidget(writed_value_t1_13, 1, 14, 1, 1);

        label_u1_11 = new QLabel(scrollAreaWidgetContents);
        label_u1_11->setObjectName(QString::fromUtf8("label_u1_11"));
        label_u1_11->setFont(font);

        gridLayout_2->addWidget(label_u1_11, 7, 12, 1, 1);

        writeButtont2_7 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_7->setObjectName(QString::fromUtf8("writeButtont2_7"));
        writeButtont2_7->setFont(font);

        gridLayout_2->addWidget(writeButtont2_7, 10, 8, 1, 1);

        writed_value_u1_5 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_5->setObjectName(QString::fromUtf8("writed_value_u1_5"));
        writed_value_u1_5->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_5, 5, 6, 1, 1);

        writeButtonu1_3 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_3->setObjectName(QString::fromUtf8("writeButtonu1_3"));
        writeButtonu1_3->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_3, 6, 4, 1, 1);

        label_u1_8 = new QLabel(scrollAreaWidgetContents);
        label_u1_8->setObjectName(QString::fromUtf8("label_u1_8"));
        label_u1_8->setFont(font);

        gridLayout_2->addWidget(label_u1_8, 7, 9, 1, 1);

        writed_value_u1_15 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u1_15->setObjectName(QString::fromUtf8("writed_value_u1_15"));
        writed_value_u1_15->setFont(font);

        gridLayout_2->addWidget(writed_value_u1_15, 5, 16, 1, 1);

        writeButtonu1_6 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu1_6->setObjectName(QString::fromUtf8("writeButtonu1_6"));
        writeButtonu1_6->setFont(font);

        gridLayout_2->addWidget(writeButtonu1_6, 6, 7, 1, 1);

        label_u1_6 = new QLabel(scrollAreaWidgetContents);
        label_u1_6->setObjectName(QString::fromUtf8("label_u1_6"));
        label_u1_6->setFont(font);

        gridLayout_2->addWidget(label_u1_6, 7, 7, 1, 1);

        writeButtonu2_4 = new QPushButton(scrollAreaWidgetContents);
        writeButtonu2_4->setObjectName(QString::fromUtf8("writeButtonu2_4"));
        writeButtonu2_4->setFont(font);

        gridLayout_2->addWidget(writeButtonu2_4, 14, 5, 1, 1);

        readed_value_U1 = new QLabel(scrollAreaWidgetContents);
        readed_value_U1->setObjectName(QString::fromUtf8("readed_value_U1"));
        readed_value_U1->setFont(font);

        gridLayout_2->addWidget(readed_value_U1, 8, 1, 1, 8);

        label_u1_15 = new QLabel(scrollAreaWidgetContents);
        label_u1_15->setObjectName(QString::fromUtf8("label_u1_15"));
        label_u1_15->setFont(font);

        gridLayout_2->addWidget(label_u1_15, 7, 16, 1, 1);

        writeButtont2_11 = new QPushButton(scrollAreaWidgetContents);
        writeButtont2_11->setObjectName(QString::fromUtf8("writeButtont2_11"));
        writeButtont2_11->setFont(font);

        gridLayout_2->addWidget(writeButtont2_11, 10, 12, 1, 1);

        writed_value_u2_14 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_14->setObjectName(QString::fromUtf8("writed_value_u2_14"));
        writed_value_u2_14->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_14, 13, 15, 1, 1);

        writed_value_u2_5 = new QLineEdit(scrollAreaWidgetContents);
        writed_value_u2_5->setObjectName(QString::fromUtf8("writed_value_u2_5"));
        writed_value_u2_5->setFont(font);

        gridLayout_2->addWidget(writed_value_u2_5, 13, 6, 1, 1);

        rewriteButton = new QPushButton(scrollAreaWidgetContents);
        rewriteButton->setObjectName(QString::fromUtf8("rewriteButton"));
        rewriteButton->setFont(font);

        gridLayout_2->addWidget(rewriteButton, 2, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);


        retranslateUi(time_voltage);

        QMetaObject::connectSlotsByName(time_voltage);
    } // setupUi

    void retranslateUi(QWidget *time_voltage)
    {
        time_voltage->setWindowTitle(QCoreApplication::translate("time_voltage", "Form", nullptr));
        writeButtont2_12->setText(QCoreApplication::translate("time_voltage", "t12", nullptr));
        readed_value_t2->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\262\321\200\320\265\320\274\320\265\320\275\320\270</span></p></body></html>", nullptr));
        label_u1_2->setText(QCoreApplication::translate("time_voltage", "U2", nullptr));
        label_t2_7->setText(QCoreApplication::translate("time_voltage", "t7", nullptr));
        writeButtonu2_8->setText(QCoreApplication::translate("time_voltage", "U8", nullptr));
        label_t1_2->setText(QCoreApplication::translate("time_voltage", "t2", nullptr));
        label_u2_1->setText(QCoreApplication::translate("time_voltage", "U1", nullptr));
        label_u1_7->setText(QCoreApplication::translate("time_voltage", "U7", nullptr));
        label_t2_3->setText(QCoreApplication::translate("time_voltage", "t3", nullptr));
        readed_value_t1->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\262\321\200\320\265\320\274\320\265\320\275\320\270</span></p></body></html>", nullptr));
        writeButtont1_9->setText(QCoreApplication::translate("time_voltage", "t9", nullptr));
        label_t2_6->setText(QCoreApplication::translate("time_voltage", "t6", nullptr));
        label_t2_14->setText(QCoreApplication::translate("time_voltage", "t14", nullptr));
        writeButtont1_1->setText(QCoreApplication::translate("time_voltage", "t1", nullptr));
        label_t2_5->setText(QCoreApplication::translate("time_voltage", "t5", nullptr));
        writeButtont2_14->setText(QCoreApplication::translate("time_voltage", "t14", nullptr));
        label_u2_8->setText(QCoreApplication::translate("time_voltage", "U8", nullptr));
        label_t2_12->setText(QCoreApplication::translate("time_voltage", "t12", nullptr));
        writeButtont1_10->setText(QCoreApplication::translate("time_voltage", "t10", nullptr));
        writeButtont2_15->setText(QCoreApplication::translate("time_voltage", "t15", nullptr));
        writeButtont2_1->setText(QCoreApplication::translate("time_voltage", "t1", nullptr));
        writeButtonu1_14->setText(QCoreApplication::translate("time_voltage", "U14", nullptr));
        label_t2_1->setText(QCoreApplication::translate("time_voltage", "t1", nullptr));
        label_t1_15->setText(QCoreApplication::translate("time_voltage", "t15", nullptr));
        writeButtont1_6->setText(QCoreApplication::translate("time_voltage", "t6", nullptr));
        writeButtonu2_14->setText(QCoreApplication::translate("time_voltage", "U14", nullptr));
        writeButtonu1_13->setText(QCoreApplication::translate("time_voltage", "U13", nullptr));
        label_U2->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">U \320\267\320\260\320\267\320\276\321\200\320\260 (\320\262\321\202\320\276\321\200\320\276\320\271 \321\215\321\202\320\260\320\277):</span></p></body></html>", nullptr));
        label_t1_12->setText(QCoreApplication::translate("time_voltage", "t12", nullptr));
        label_u1_5->setText(QCoreApplication::translate("time_voltage", "U5", nullptr));
        writeButtonu1_9->setText(QCoreApplication::translate("time_voltage", "U9", nullptr));
        label_t1_14->setText(QCoreApplication::translate("time_voltage", "t14", nullptr));
        writeButtont2_9->setText(QCoreApplication::translate("time_voltage", "t9", nullptr));
        writeButtont1_11->setText(QCoreApplication::translate("time_voltage", "t11", nullptr));
        writeButtonu2_13->setText(QCoreApplication::translate("time_voltage", "U13", nullptr));
        writeButtont1_14->setText(QCoreApplication::translate("time_voltage", "t14", nullptr));
        label_t1_3->setText(QCoreApplication::translate("time_voltage", "t3", nullptr));
        writeButtont2_6->setText(QCoreApplication::translate("time_voltage", "t6", nullptr));
        writeButtonu1_12->setText(QCoreApplication::translate("time_voltage", "U12", nullptr));
        writeButtont2_8->setText(QCoreApplication::translate("time_voltage", "t8", nullptr));
        label_t1_7->setText(QCoreApplication::translate("time_voltage", "t7", nullptr));
        label_u2_5->setText(QCoreApplication::translate("time_voltage", "U5", nullptr));
        label_u1_1->setText(QCoreApplication::translate("time_voltage", "U1", nullptr));
        writeButtonu2_5->setText(QCoreApplication::translate("time_voltage", "U5", nullptr));
        label_u1_4->setText(QCoreApplication::translate("time_voltage", "U4", nullptr));
        label_t1_4->setText(QCoreApplication::translate("time_voltage", "t4", nullptr));
        label_u2_14->setText(QCoreApplication::translate("time_voltage", "U14", nullptr));
        writeButtont1_15->setText(QCoreApplication::translate("time_voltage", "t15", nullptr));
        label_u2_13->setText(QCoreApplication::translate("time_voltage", "U13", nullptr));
        label_u2_4->setText(QCoreApplication::translate("time_voltage", "U4", nullptr));
        label_u2_2->setText(QCoreApplication::translate("time_voltage", "U2", nullptr));
        label_t1_13->setText(QCoreApplication::translate("time_voltage", "t13", nullptr));
        label_u1_12->setText(QCoreApplication::translate("time_voltage", "U12", nullptr));
        label_u1_14->setText(QCoreApplication::translate("time_voltage", "U14", nullptr));
        label_t1_8->setText(QCoreApplication::translate("time_voltage", "t8", nullptr));
        writeButtonu2_2->setText(QCoreApplication::translate("time_voltage", "U2", nullptr));
        writeButtonu1_1->setText(QCoreApplication::translate("time_voltage", "U1", nullptr));
        writeButtonu2_11->setText(QCoreApplication::translate("time_voltage", "U11", nullptr));
        writeButtonu1_8->setText(QCoreApplication::translate("time_voltage", "U8", nullptr));
        writeButtonu2_7->setText(QCoreApplication::translate("time_voltage", "U7", nullptr));
        writeButtont2_10->setText(QCoreApplication::translate("time_voltage", "t10", nullptr));
        writeButtonu1_7->setText(QCoreApplication::translate("time_voltage", "U7", nullptr));
        writeButtonu2_12->setText(QCoreApplication::translate("time_voltage", "U12", nullptr));
        label_t2_11->setText(QCoreApplication::translate("time_voltage", "t11", nullptr));
        label_t2_2->setText(QCoreApplication::translate("time_voltage", "t2", nullptr));
        writeButtont2_2->setText(QCoreApplication::translate("time_voltage", "t2", nullptr));
        label_u2_9->setText(QCoreApplication::translate("time_voltage", "U9", nullptr));
        label_u1_3->setText(QCoreApplication::translate("time_voltage", "U3", nullptr));
        label_u1_10->setText(QCoreApplication::translate("time_voltage", "U10", nullptr));
        writeButtont1_7->setText(QCoreApplication::translate("time_voltage", "t7", nullptr));
        writeButtonu1_2->setText(QCoreApplication::translate("time_voltage", "U2", nullptr));
        writeButtonu2_1->setText(QCoreApplication::translate("time_voltage", "U1", nullptr));
        label_u1_9->setText(QCoreApplication::translate("time_voltage", "U9", nullptr));
        label_u2_3->setText(QCoreApplication::translate("time_voltage", "U3", nullptr));
        label_t2_13->setText(QCoreApplication::translate("time_voltage", "t13", nullptr));
        label_u2_15->setText(QCoreApplication::translate("time_voltage", "U15", nullptr));
        label_t2_15->setText(QCoreApplication::translate("time_voltage", "t15", nullptr));
        writeButtont1_13->setText(QCoreApplication::translate("time_voltage", "t13", nullptr));
        writeButtont1_8->setText(QCoreApplication::translate("time_voltage", "t8", nullptr));
        writeButtonu1_11->setText(QCoreApplication::translate("time_voltage", "U11", nullptr));
        label_t1_1->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p>t1</p></body></html>", nullptr));
        writeButtont2_3->setText(QCoreApplication::translate("time_voltage", "t3", nullptr));
        label_t1_11->setText(QCoreApplication::translate("time_voltage", "t11", nullptr));
        writeButtonu1_5->setText(QCoreApplication::translate("time_voltage", "U5", nullptr));
        writeButtonu2_6->setText(QCoreApplication::translate("time_voltage", "U6", nullptr));
        label_t1->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\222\321\200\320\265\320\274\321\217 (\320\276\321\202\321\201\321\207\320\270\321\202\321\213\320\262\320\260\320\265\321\202\321\201\321\217 \320\276\321\202 \320\241\321\202\320\260\321\200\321\202 I):</span></p></body></html>", nullptr));
        label_u2_11->setText(QCoreApplication::translate("time_voltage", "U11", nullptr));
        writeButtont1_4->setText(QCoreApplication::translate("time_voltage", "t4", nullptr));
        label_t1_5->setText(QCoreApplication::translate("time_voltage", "t5", nullptr));
        writeButtont1_3->setText(QCoreApplication::translate("time_voltage", "t3", nullptr));
        label_u2_10->setText(QCoreApplication::translate("time_voltage", "U10", nullptr));
        label_t2_8->setText(QCoreApplication::translate("time_voltage", "t8", nullptr));
        writeButtonu2_3->setText(QCoreApplication::translate("time_voltage", "U3", nullptr));
        label_t1_6->setText(QCoreApplication::translate("time_voltage", "t6", nullptr));
        writeButtonu1_15->setText(QCoreApplication::translate("time_voltage", "U15", nullptr));
        label_u2_6->setText(QCoreApplication::translate("time_voltage", "U6", nullptr));
        writeButtont1_2->setText(QCoreApplication::translate("time_voltage", "t2", nullptr));
        writeButtonu1_4->setText(QCoreApplication::translate("time_voltage", "U4", nullptr));
        label->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p>\320\230\321\201\320\277\320\276\320\273\321\214\320\267\321\203\320\271\321\202\320\265 \321\202\320\276\320\273\321\214\320\272\320\276 \321\206\320\270\321\204\321\200\321\213 \320\270 \321\202\320\276\321\207\320\272\321\203.</p></body></html>", nullptr));
        writeButtonu2_15->setText(QCoreApplication::translate("time_voltage", "U15", nullptr));
        writeButtonu2_9->setText(QCoreApplication::translate("time_voltage", "U9", nullptr));
        label_U1->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">U \320\267\320\260\320\267\320\276\321\200\320\260 (\320\277\320\265\321\200\320\262\321\213\320\271 \321\215\321\202\320\260\320\277):</span></p></body></html>", nullptr));
        writeButtont2_5->setText(QCoreApplication::translate("time_voltage", "t5", nullptr));
        writeButtonu2_10->setText(QCoreApplication::translate("time_voltage", "U10", nullptr));
        writeButtont2_13->setText(QCoreApplication::translate("time_voltage", "t13", nullptr));
        label_t1_10->setText(QCoreApplication::translate("time_voltage", "t10", nullptr));
        label_t1_9->setText(QCoreApplication::translate("time_voltage", "t9", nullptr));
        label_t2_9->setText(QCoreApplication::translate("time_voltage", "t9", nullptr));
        writeButtont1_5->setText(QCoreApplication::translate("time_voltage", "t5", nullptr));
        readed_value_U2->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 U</span></p></body></html>", nullptr));
        label_u2_7->setText(QCoreApplication::translate("time_voltage", "U7", nullptr));
        label_u1_13->setText(QCoreApplication::translate("time_voltage", "U13", nullptr));
        writeButtonu1_10->setText(QCoreApplication::translate("time_voltage", "U10", nullptr));
        writeButtont2_4->setText(QCoreApplication::translate("time_voltage", "t4", nullptr));
        writeButtont1_12->setText(QCoreApplication::translate("time_voltage", "t12", nullptr));
        label_t2->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\222\321\200\320\265\320\274\321\217 (\320\276\321\202\321\201\321\207\320\270\321\202\321\213\320\262\320\260\320\265\321\202\321\201\321\217 \320\276\321\202 \320\241\321\202\320\260\321\200\321\202 II):</span></p></body></html>", nullptr));
        label_t2_4->setText(QCoreApplication::translate("time_voltage", "t4", nullptr));
        label_t2_10->setText(QCoreApplication::translate("time_voltage", "t10", nullptr));
        label_u2_12->setText(QCoreApplication::translate("time_voltage", "U12", nullptr));
        label_u1_11->setText(QCoreApplication::translate("time_voltage", "U11", nullptr));
        writeButtont2_7->setText(QCoreApplication::translate("time_voltage", "t7", nullptr));
        writeButtonu1_3->setText(QCoreApplication::translate("time_voltage", "U3", nullptr));
        label_u1_8->setText(QCoreApplication::translate("time_voltage", "U8", nullptr));
        writeButtonu1_6->setText(QCoreApplication::translate("time_voltage", "U6", nullptr));
        label_u1_6->setText(QCoreApplication::translate("time_voltage", "U6", nullptr));
        writeButtonu2_4->setText(QCoreApplication::translate("time_voltage", "U4", nullptr));
        readed_value_U1->setText(QCoreApplication::translate("time_voltage", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\241\321\207\320\270\321\202\320\260\320\275\320\275\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 U</span></p></body></html>", nullptr));
        label_u1_15->setText(QCoreApplication::translate("time_voltage", "U15", nullptr));
        writeButtont2_11->setText(QCoreApplication::translate("time_voltage", "t11", nullptr));
        rewriteButton->setText(QCoreApplication::translate("time_voltage", "\320\237\320\265\321\200\320\265\320\267\320\260\320\277\320\270\321\201\320\260\321\202\321\214 \321\204\320\260\320\271\320\273\321\213", nullptr));
    } // retranslateUi

};

namespace Ui {
    class time_voltage: public Ui_time_voltage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIME_VOLTAGE__H
