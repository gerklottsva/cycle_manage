QT       += core gui printsupport
QT+=core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    b_k.cpp \
    coefficients.cpp \
    graph.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    start_cycle.cpp \
    time_voltage.cpp

HEADERS += \
    b_k.h \
    coefficients.h \
    graph.h \
    mainwindow.h \
    qcustomplot.h \
    start_cycle.h \
    time_voltage.h

FORMS += \
    b_k.ui \
    coefficients.ui \
    graph.ui \
    mainwindow.ui \
    start_cycle.ui \
    time_voltage.ui

win32:RC_FILE=file.rc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
