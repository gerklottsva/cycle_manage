#ifndef B_K_H
#define B_K_H

#include <QWidget>
#include "ui_b_k.h"
#include <QString>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <QRegExp>

namespace Ui {
class b_k;
}

class b_k : public QWidget
{
    Q_OBJECT

public:
    explicit b_k(QWidget *parent = nullptr);
    ~b_k();

public slots:
    void ui_write_to_file_B0();
    void ui_write_to_file_K();
    void plus_value_B0();
    void minus_value_B0();
    void oneplus_value_B0();
    void oneminus_value_B0();
    void twoplus_value_B0();
    void twominus_value_B0();
    void threeplus_value_B0();
    void threeminus_value_B0();
    void plus_value_K();
    void minus_value_K();
    void oneplus_value_K();
    void oneminus_value_K();
    void twoplus_value_K();
    void twominus_value_K();
    void threeplus_value_K();
    void threeminus_value_K();

private:
    Ui::b_k *ui;
};

#endif // B_K_H
