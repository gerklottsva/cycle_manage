#include "time_voltage.h"

std::vector<std::string> res_points;
std::string find_stage1 = "U_HV1Stage1_points = [";
std::string find_stage2 = "U_HV1Stage2_points = [";
std::string path_to_points = "D:\\RF station/Tables/main generatetables_09.09.2021/generatetables.py";

void Read_File_Points()
{
    res_points.clear();
    std::fstream file_Points;
    file_Points.open(path_to_points, std::fstream::in);

    std::string textLine;
    while (getline(file_Points, textLine))
    {
        res_points.push_back(textLine);
    }

    file_Points.close();
}

QString Read_t1()
{
        auto it_t = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage1) != std::string::npos;
            });

        return(QString::fromStdString(*(it_t+1)));
}

QString Read_U1()
{
        auto it_t = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage1) != std::string::npos;
            });

        return(QString::fromStdString(*(it_t+2)));
}

QString Read_t2()
{
        auto it_t = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage2) != std::string::npos;
            });

        return(QString::fromStdString(*(it_t+1)));
}

QString Read_U2()
{
        auto it_t = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage2) != std::string::npos;
            });

        return(QString::fromStdString(*(it_t+2)));
}

void Write_t1(std::string value)
{
        auto it_t1 = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage1) != std::string::npos;
            });

        if ((it_t1+1) != res_points.end()) (it_t1+1)->clear();

        std::stringstream string_t1;
        string_t1 << value;
        *(it_t1+1) = string_t1.str();
}

void Write_U1(std::string value)
{
        auto it_t1 = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage1) != std::string::npos;
            });

        if ((it_t1+2) != res_points.end()) (it_t1+2)->clear();

        std::stringstream string_u1;
        string_u1 << value;
        *(it_t1+2) = string_u1.str();
}

void Write_t2(std::string value)
{
        auto it_t1 = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage2) != std::string::npos;
            });

        if ((it_t1+1) != res_points.end()) (it_t1+1)->clear();

        std::stringstream string_t2;
        string_t2 << value;
        *(it_t1+1) = string_t2.str();
}

void Write_U2(std::string value)
{
        auto it_t1 = std::find_if(res_points.begin(), res_points.end(), [&](std::string& var)
            {
                return var.find(find_stage2) != std::string::npos;
            });

        if ((it_t1+2) != res_points.end()) (it_t1+2)->clear();

        std::stringstream string_u2;
        string_u2 << value;
        *(it_t1+2) = string_u2.str();
}

void Write_To_File_Points()
{
    std::fstream file_Points;
    file_Points.open(path_to_points, std::fstream::out | std::ofstream::trunc);
    if (file_Points.is_open())
    {
    int k = 0;
        for (auto& var : res_points)
      {
         if (k++ > 0)  file_Points << std::endl;
         file_Points << var;
      }
    }
    file_Points.close();
}

time_voltage::time_voltage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::time_voltage)
{
    ui->setupUi(this);

    setWindowTitle("Изменение времени и напряжений");

    Read_File_Points();

    ui->readed_value_t1->setText(Read_t1());
    ui->readed_value_U1->setText(Read_U1());
    ui->readed_value_t2->setText(Read_t2());
    ui->readed_value_U2->setText(Read_U2());
    ui_read_t1_1();
    ui_read_t1_2to14();
    ui_read_t1_15();
    ui_read_u1_1();
    ui_read_u1_2to14();
    ui_read_u1_15();
    ui_read_t2_1();
    ui_read_t2_2to14();
    ui_read_t2_15();
    ui_read_u2_1();
    ui_read_u2_2to14();
    ui_read_u2_15();

    connect(ui->writeButtont1_1, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_1()));
    connect(ui->writeButtont1_2, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_2()));
    connect(ui->writeButtont1_3, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_3()));
    connect(ui->writeButtont1_4, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_4()));
    connect(ui->writeButtont1_5, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_5()));
    connect(ui->writeButtont1_6, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_6()));
    connect(ui->writeButtont1_7, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_7()));
    connect(ui->writeButtont1_8, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_8()));
    connect(ui->writeButtont1_9, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_9()));
    connect(ui->writeButtont1_10, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_10()));
    connect(ui->writeButtont1_11, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_11()));
    connect(ui->writeButtont1_12, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_12()));
    connect(ui->writeButtont1_13, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_13()));
    connect(ui->writeButtont1_14, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_14()));
    connect(ui->writeButtont1_15, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t1_15()));

    connect(ui->writeButtonu1_1, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_1()));
    connect(ui->writeButtonu1_2, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_2()));
    connect(ui->writeButtonu1_3, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_3()));
    connect(ui->writeButtonu1_4, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_4()));
    connect(ui->writeButtonu1_5, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_5()));
    connect(ui->writeButtonu1_6, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_6()));
    connect(ui->writeButtonu1_7, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_7()));
    connect(ui->writeButtonu1_8, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_8()));
    connect(ui->writeButtonu1_9, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_9()));
    connect(ui->writeButtonu1_10, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_10()));
    connect(ui->writeButtonu1_11, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_11()));
    connect(ui->writeButtonu1_12, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_12()));
    connect(ui->writeButtonu1_13, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_13()));
    connect(ui->writeButtonu1_14, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_14()));
    connect(ui->writeButtonu1_15, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u1_15()));

    connect(ui->writeButtont2_1, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_1()));
    connect(ui->writeButtont2_2, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_2()));
    connect(ui->writeButtont2_3, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_3()));
    connect(ui->writeButtont2_4, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_4()));
    connect(ui->writeButtont2_5, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_5()));
    connect(ui->writeButtont2_6, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_6()));
    connect(ui->writeButtont2_7, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_7()));
    connect(ui->writeButtont2_8, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_8()));
    connect(ui->writeButtont2_9, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_9()));
    connect(ui->writeButtont2_10, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_10()));
    connect(ui->writeButtont2_11, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_11()));
    connect(ui->writeButtont2_12, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_12()));
    connect(ui->writeButtont2_13, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_13()));
    connect(ui->writeButtont2_14, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_14()));
    connect(ui->writeButtont2_15, SIGNAL(clicked()), this, SLOT(ui_write_to_file_t2_15()));

    connect(ui->writeButtonu2_1, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_1()));
    connect(ui->writeButtonu2_2, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_2()));
    connect(ui->writeButtonu2_3, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_3()));
    connect(ui->writeButtonu2_4, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_4()));
    connect(ui->writeButtonu2_5, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_5()));
    connect(ui->writeButtonu2_6, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_6()));
    connect(ui->writeButtonu2_7, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_7()));
    connect(ui->writeButtonu2_8, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_8()));
    connect(ui->writeButtonu2_9, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_9()));
    connect(ui->writeButtonu2_10, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_10()));
    connect(ui->writeButtonu2_11, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_11()));
    connect(ui->writeButtonu2_12, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_12()));
    connect(ui->writeButtonu2_13, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_13()));
    connect(ui->writeButtonu2_14, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_14()));
    connect(ui->writeButtonu2_15, SIGNAL(clicked()), this, SLOT(ui_write_to_file_u2_15()));
}

time_voltage::~time_voltage()
{
    delete ui;
}

void time_voltage::ui_read_t1_1()
{
    QString qstr = ui-> readed_value_t1->text();
    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
        {
            if ( ( qstr )[i] == ',')
                {
                    if(count==1)
                        {
                            n1=i;
                        }
                    count++;
                }
        }
    ui->label_t1_1->setText(qstr.mid(1, n1-1));
}

void time_voltage::ui_read_t1_2to14()
{
    QString qstr = ui-> readed_value_t1->text();
    int n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
         switch(count){
         case 1:
             n1=i;
             break;
         case 2:
             n2=i;
             break;
         case 3:
             n3 = i;
             break;
         case 4:
             n4 = i;
             break;
         case 5:
             n5 = i;
             break;
         case 6:
             n6 = i;
             break;
         case 7:
             n7 = i;
             break;
         case 8:
             n8 = i;
             break;
         case 9:
             n9 = i;
             break;
         case 10:
             n10 = i;
             break;
         case 11:
             n11 = i;
             break;
         case 12:
             n12 = i;
             break;
         case 13:
             n13 = i;
             break;
         case 14:
             n14 = i;
             break;
         }
            count++;
        }
    }
    ui->label_t1_2->setText(qstr.mid(n1+1,n2-n1-1));
    ui->label_t1_3->setText(qstr.mid(n2+1,n3-n2-1));
    ui->label_t1_4->setText(qstr.mid(n3+1,n4-n3-1));
    ui->label_t1_5->setText(qstr.mid(n4+1,n5-n4-1));
    ui->label_t1_6->setText(qstr.mid(n5+1,n6-n5-1));
    ui->label_t1_7->setText(qstr.mid(n6+1,n7-n6-1));
    ui->label_t1_8->setText(qstr.mid(n7+1,n8-n7-1));
    ui->label_t1_9->setText(qstr.mid(n8+1,n9-n8-1));
    ui->label_t1_10->setText(qstr.mid(n9+1,n10-n9-1));
    ui->label_t1_11->setText(qstr.mid(n10+1,n11-n10-1));
    ui->label_t1_12->setText(qstr.mid(n11+1,n12-n11-1));
    ui->label_t1_13->setText(qstr.mid(n12+1,n13-n12-1));
    ui->label_t1_14->setText(qstr.mid(n13+1,n14-n13-1));
}

void time_voltage::ui_read_t1_15()
{
    QString qstr = ui-> readed_value_t1->text();
    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
            if(count==14)
              {
                  n1=i;
              }
               count++;
           }
        if ( ( qstr )[i] == ']')
           {
            if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    ui->label_t1_15->setText(qstr.mid(n1+1,n2-n1-1));
}

void time_voltage::ui_read_t2_1()
{
    QString qstr = ui-> readed_value_t2->text();
    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
        {
            if ( ( qstr )[i] == ',')
                {
                    if(count==1)
                        {
                            n1=i;
                        }
                    count++;
                }
        }
    ui->label_t2_1->setText(qstr.mid(1, n1-1));
}

void time_voltage::ui_read_t2_2to14()
{
    QString qstr = ui-> readed_value_t2->text();
    int n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
         switch(count){
         case 1:
             n1=i;
             break;
         case 2:
             n2=i;
             break;
         case 3:
             n3 = i;
             break;
         case 4:
             n4 = i;
             break;
         case 5:
             n5 = i;
             break;
         case 6:
             n6 = i;
             break;
         case 7:
             n7 = i;
             break;
         case 8:
             n8 = i;
             break;
         case 9:
             n9 = i;
             break;
         case 10:
             n10 = i;
             break;
         case 11:
             n11 = i;
             break;
         case 12:
             n12 = i;
             break;
         case 13:
             n13 = i;
             break;
         case 14:
             n14 = i;
             break;
         }
            count++;
        }
    }
    ui->label_t2_2->setText(qstr.mid(n1+1,n2-n1-1));
    ui->label_t2_3->setText(qstr.mid(n2+1,n3-n2-1));
    ui->label_t2_4->setText(qstr.mid(n3+1,n4-n3-1));
    ui->label_t2_5->setText(qstr.mid(n4+1,n5-n4-1));
    ui->label_t2_6->setText(qstr.mid(n5+1,n6-n5-1));
    ui->label_t2_7->setText(qstr.mid(n6+1,n7-n6-1));
    ui->label_t2_8->setText(qstr.mid(n7+1,n8-n7-1));
    ui->label_t2_9->setText(qstr.mid(n8+1,n9-n8-1));
    ui->label_t2_10->setText(qstr.mid(n9+1,n10-n9-1));
    ui->label_t2_11->setText(qstr.mid(n10+1,n11-n10-1));
    ui->label_t2_12->setText(qstr.mid(n11+1,n12-n11-1));
    ui->label_t2_13->setText(qstr.mid(n12+1,n13-n12-1));
    ui->label_t2_14->setText(qstr.mid(n13+1,n14-n13-1));
}

void time_voltage::ui_read_t2_15()
{
    QString qstr = ui-> readed_value_t2->text();
    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
            if(count==14)
              {
                  n1=i;
              }
               count++;
           }
        if ( ( qstr )[i] == ']')
           {
            if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    ui->label_t2_15->setText(qstr.mid(n1+1,n2-n1-1));
}

void time_voltage::ui_read_u1_1()
{
    QString qstr = ui-> readed_value_U1->text();
    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
        {
            if ( ( qstr )[i] == ',')
                {
                    if(count==1)
                        {
                            n1=i;
                        }
                    count++;
                }
        }
    ui->label_u1_1->setText(qstr.mid(1, n1-1));
}

void time_voltage::ui_read_u1_2to14()
{
    QString qstr = ui-> readed_value_U1->text();
    int n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
         switch(count){
         case 1:
             n1=i;
             break;
         case 2:
             n2=i;
             break;
         case 3:
             n3 = i;
             break;
         case 4:
             n4 = i;
             break;
         case 5:
             n5 = i;
             break;
         case 6:
             n6 = i;
             break;
         case 7:
             n7 = i;
             break;
         case 8:
             n8 = i;
             break;
         case 9:
             n9 = i;
             break;
         case 10:
             n10 = i;
             break;
         case 11:
             n11 = i;
             break;
         case 12:
             n12 = i;
             break;
         case 13:
             n13 = i;
             break;
         case 14:
             n14 = i;
             break;
         }
            count++;
        }
    }
    ui->label_u1_2->setText(qstr.mid(n1+1,n2-n1-1));
    ui->label_u1_3->setText(qstr.mid(n2+1,n3-n2-1));
    ui->label_u1_4->setText(qstr.mid(n3+1,n4-n3-1));
    ui->label_u1_5->setText(qstr.mid(n4+1,n5-n4-1));
    ui->label_u1_6->setText(qstr.mid(n5+1,n6-n5-1));
    ui->label_u1_7->setText(qstr.mid(n6+1,n7-n6-1));
    ui->label_u1_8->setText(qstr.mid(n7+1,n8-n7-1));
    ui->label_u1_9->setText(qstr.mid(n8+1,n9-n8-1));
    ui->label_u1_10->setText(qstr.mid(n9+1,n10-n9-1));
    ui->label_u1_11->setText(qstr.mid(n10+1,n11-n10-1));
    ui->label_u1_12->setText(qstr.mid(n11+1,n12-n11-1));
    ui->label_u1_13->setText(qstr.mid(n12+1,n13-n12-1));
    ui->label_u1_14->setText(qstr.mid(n13+1,n14-n13-1));
}

void time_voltage::ui_read_u1_15()
{
    QString qstr = ui-> readed_value_U1->text();
    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
            if(count==14)
              {
                  n1=i;
              }
               count++;
           }
        if ( ( qstr )[i] == ']')
           {
            if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    ui->label_u1_15->setText(qstr.mid(n1+1,n2-n1-1));
}

void time_voltage::ui_read_u2_1()
{
    QString qstr = ui-> readed_value_U2->text();
    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
        {
            if ( ( qstr )[i] == ',')
                {
                    if(count==1)
                        {
                            n1=i;
                        }
                    count++;
                }
        }
    ui->label_u2_1->setText(qstr.mid(1, n1-1));
}

void time_voltage::ui_read_u2_2to14()
{
    QString qstr = ui-> readed_value_U2->text();
    int n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
         switch(count){
         case 1:
             n1=i;
             break;
         case 2:
             n2=i;
             break;
         case 3:
             n3 = i;
             break;
         case 4:
             n4 = i;
             break;
         case 5:
             n5 = i;
             break;
         case 6:
             n6 = i;
             break;
         case 7:
             n7 = i;
             break;
         case 8:
             n8 = i;
             break;
         case 9:
             n9 = i;
             break;
         case 10:
             n10 = i;
             break;
         case 11:
             n11 = i;
             break;
         case 12:
             n12 = i;
             break;
         case 13:
             n13 = i;
             break;
         case 14:
             n14 = i;
             break;
         }
            count++;
        }
    }
    ui->label_u2_2->setText(qstr.mid(n1+1,n2-n1-1));
    ui->label_u2_3->setText(qstr.mid(n2+1,n3-n2-1));
    ui->label_u2_4->setText(qstr.mid(n3+1,n4-n3-1));
    ui->label_u2_5->setText(qstr.mid(n4+1,n5-n4-1));
    ui->label_u2_6->setText(qstr.mid(n5+1,n6-n5-1));
    ui->label_u2_7->setText(qstr.mid(n6+1,n7-n6-1));
    ui->label_u2_8->setText(qstr.mid(n7+1,n8-n7-1));
    ui->label_u2_9->setText(qstr.mid(n8+1,n9-n8-1));
    ui->label_u2_10->setText(qstr.mid(n9+1,n10-n9-1));
    ui->label_u2_11->setText(qstr.mid(n10+1,n11-n10-1));
    ui->label_u2_12->setText(qstr.mid(n11+1,n12-n11-1));
    ui->label_u2_13->setText(qstr.mid(n12+1,n13-n12-1));
    ui->label_u2_14->setText(qstr.mid(n13+1,n14-n13-1));
}

void time_voltage::ui_read_u2_15()
{
    QString qstr = ui-> readed_value_U2->text();
    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
            if(count==14)
              {
                  n1=i;
              }
               count++;
           }
        if ( ( qstr )[i] == ']')
           {
            if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    ui->label_u2_15->setText(qstr.mid(n1+1,n2-n1-1));
}

void time_voltage::ui_write_to_file_t1_1()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_1->text();

    Read_File_Points();

    bool b;
    int c = 0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
            {
                if ( ( qstr_old )[i] == ',')
                    {
                        if(count==1)
                            {
                                n1=i;
                            }
                        count++;
                    }
            }

        qstr_old.replace(1,n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_1();
}

void time_voltage::ui_write_to_file_t1_2()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_2->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==1)
             {
                 n1=i;
             }
               if(count==2)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_3()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_3->text();
    Read_File_Points();


    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==2)
             {
                 n1=i;
             }
               if(count==3)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_4()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_4->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==3)
             {
                 n1=i;
             }
               if(count==4)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_5()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_5->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==4)
             {
                 n1=i;
             }
               if(count==5)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_6()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_6->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==5)
             {
                 n1=i;
             }
               if(count==6)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_7()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_7->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==6)
             {
                 n1=i;
             }
               if(count==7)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_8()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_8->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==7)
             {
                 n1=i;
             }
               if(count==8)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_9()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_9->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==8)
             {
                 n1=i;
             }
               if(count==9)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_10()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_10->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==9)
             {
                 n1=i;
             }
               if(count==10)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_11()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_11->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==10)
             {
                 n1=i;
             }
               if(count==11)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_12()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_12->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==11)
             {
                 n1=i;
             }
               if(count==12)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_13()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_13->text();
    Read_File_Points();
    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==12)
             {
                 n1=i;
             }
               if(count==13)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_14()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_14->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==13)
             {
                 n1=i;
             }
               if(count==14)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_2to14();
}

void time_voltage::ui_write_to_file_t1_15()
{
    QString qstr_old = ui->readed_value_t1->text();
    QString qstr_new = ui->writed_value_t1_15->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

            if ( ( qstr_old )[i] == ',')
               {
                if(count==14)
                  {
                      n1=i;
                  }
                   count++;
               }
            if ( ( qstr_old )[i] == ']')
               {
                if(count==15)
                  {
                      n2=i;
                  }
                   count++;
               }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t1->setText(Read_t1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t1_15();
}

void time_voltage::ui_write_to_file_t2_1()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_1->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
            {
                if ( ( qstr_old )[i] == ',')
                    {
                        if(count==1)
                            {
                                n1=i;
                            }
                        count++;
                    }
            }

        qstr_old.replace(1,n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_1();
}

void time_voltage::ui_write_to_file_t2_2()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_2->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==1)
             {
                 n1=i;
             }
               if(count==2)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_3()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_3->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==2)
             {
                 n1=i;
             }
               if(count==3)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_4()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_4->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==3)
             {
                 n1=i;
             }
               if(count==4)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_5()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_5->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==4)
             {
                 n1=i;
             }
               if(count==5)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_6()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_6->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==5)
             {
                 n1=i;
             }
               if(count==6)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_7()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_7->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==6)
             {
                 n1=i;
             }
               if(count==7)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_8()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_8->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==7)
             {
                 n1=i;
             }
               if(count==8)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_9()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_9->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==8)
             {
                 n1=i;
             }
               if(count==9)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_10()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_10->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==9)
             {
                 n1=i;
             }
               if(count==10)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_11()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_11->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==10)
             {
                 n1=i;
             }
               if(count==11)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_12()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_12->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==11)
             {
                 n1=i;
             }
               if(count==12)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_13()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_13->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==12)
             {
                 n1=i;
             }
               if(count==13)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_14()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_14->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==13)
             {
                 n1=i;
             }
               if(count==14)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_2to14();
}

void time_voltage::ui_write_to_file_t2_15()
{
    QString qstr_old = ui->readed_value_t2->text();
    QString qstr_new = ui->writed_value_t2_15->text();
    Read_File_Points();


    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

            if ( ( qstr_old )[i] == ',')
               {
                if(count==14)
                  {
                      n1=i;
                  }
                   count++;
               }
            if ( ( qstr_old )[i] == ']')
               {
                if(count==15)
                  {
                      n2=i;
                  }
                   count++;
               }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_t2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_t2->setText(Read_t2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_t2_15();
}

void time_voltage::ui_write_to_file_u1_1()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_1->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
            {
                if ( ( qstr_old )[i] == ',')
                    {
                        if(count==1)
                            {
                                n1=i;
                            }
                        count++;
                    }
            }

        qstr_old.replace(1,n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_1();
}

void time_voltage::ui_write_to_file_u1_2()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_2->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==1)
             {
                 n1=i;
             }
               if(count==2)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_3()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_3->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==2)
             {
                 n1=i;
             }
               if(count==3)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_4()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_4->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==3)
             {
                 n1=i;
             }
               if(count==4)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_5()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_5->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==4)
             {
                 n1=i;
             }
               if(count==5)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_6()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_6->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==5)
             {
                 n1=i;
             }
               if(count==6)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_7()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_7->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==6)
             {
                 n1=i;
             }
               if(count==7)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_8()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_8->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==7)
             {
                 n1=i;
             }
               if(count==8)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_9()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_9->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==8)
             {
                 n1=i;
             }
               if(count==9)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_10()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_10->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==9)
             {
                 n1=i;
             }
               if(count==10)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_11()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_11->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==10)
             {
                 n1=i;
             }
               if(count==11)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_12()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_12->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==11)
             {
                 n1=i;
             }
               if(count==12)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_13()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_13->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==12)
             {
                 n1=i;
             }
               if(count==13)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_14()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_14->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==13)
             {
                 n1=i;
             }
               if(count==14)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_2to14();
}

void time_voltage::ui_write_to_file_u1_15()
{
    QString qstr_old = ui->readed_value_U1->text();
    QString qstr_new = ui->writed_value_u1_15->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {
            if ( ( qstr_old )[i] == ',')
               {
                if(count==14)
                  {
                      n1=i;
                  }
                   count++;
               }
            if ( ( qstr_old )[i] == ']')
               {
                if(count==15)
                  {
                      n2=i;
                  }
                   count++;
               }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U1(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U1->setText(Read_U1());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u1_15();
}

void time_voltage::ui_write_to_file_u2_1()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_1->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
            {
                if ( ( qstr_old )[i] == ',')
                    {
                        if(count==1)
                            {
                                n1=i;
                            }
                        count++;
                    }
            }

        qstr_old.replace(1,n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_1();
}

void time_voltage::ui_write_to_file_u2_2()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_2->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==1)
             {
                 n1=i;
             }
               if(count==2)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_3()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_3->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==2)
             {
                 n1=i;
             }
               if(count==3)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_4()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_4->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==3)
             {
                 n1=i;
             }
               if(count==4)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_5()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_5->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==4)
             {
                 n1=i;
             }
               if(count==5)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_6()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_6->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==5)
             {
                 n1=i;
             }
               if(count==6)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_7()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_7->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==6)
             {
                 n1=i;
             }
               if(count==7)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_8()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_8->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==7)
             {
                 n1=i;
             }
               if(count==8)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_9()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_9->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==8)
             {
                 n1=i;
             }
               if(count==9)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_10()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_10->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==9)
             {
                 n1=i;
             }
               if(count==10)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_11()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_11->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==10)
             {
                 n1=i;
             }
               if(count==11)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_12()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_12->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==11)
             {
                 n1=i;
             }
               if(count==12)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_13()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_13->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==12)
             {
                 n1=i;
             }
               if(count==13)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_14()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_14->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {

         if ( ( qstr_old )[i] == ',')
            {
             if(count==13)
             {
                 n1=i;
             }
               if(count==14)
               {
                   n2=i;
               }
                count++;
            }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_2to14();
}

void time_voltage::ui_write_to_file_u2_15()
{
    QString qstr_old = ui->readed_value_U2->text();
    QString qstr_new = ui->writed_value_u2_15->text();
    Read_File_Points();

    bool b;
    int c=0;

    if(qstr_new.isEmpty())
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    else
    {
    for(int i = 0; i<qstr_new.size();i++)
    {
        if(!qstr_new[i].isNumber())
        {
            if(qstr_new[0]=='-'&&qstr_new[1].isNumber())
            {
                b = true;
            }
            else if(qstr_new[i]=='.')
            {
                b = true;
                c++;
            }
            else
            {
                b = false;
                break;
            }
        }
    }
    if(b==true&&c<2&&qstr_new.at(qstr_new.size()-1)!='.'&&qstr_new.at(0)!='.')
    {
        ui->label->setText("Используйте только цифры и точку.");
        int n1 = 0;
        int n2 = 0;
        int count = 1;
        for ( int i = 0; i != qstr_old.size(); i++ )
        {
            if ( ( qstr_old )[i] == ',')
               {
                if(count==14)
                  {
                      n1=i;
                  }
                   count++;
               }
            if ( ( qstr_old )[i] == ']')
               {
                if(count==15)
                  {
                      n2=i;
                  }
                   count++;
               }
        }
        qstr_old.replace(n1+1,n2-n1-1,qstr_new);
        Write_U2(qstr_old.toStdString());
        Write_To_File_Points();
        ui->readed_value_U2->setText(Read_U2());
    }
    else
    {
        ui->label->setText("Используйте только цифры и точку!");
    }
    }
    ui_read_u2_15();
}

QString time_voltage::ui_read_from_file_t1_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
            count++;
        }
    }
    return qstr.mid(1,n1-1);
}

QString time_voltage::ui_read_from_file_t2_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
           if(count==2)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t3_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==2)
           {
               n1=i;
           }
           if(count==3)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t4_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==3)
           {
               n1=i;
           }
           if(count==4)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t5_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==4)
           {
               n1=i;
           }
           if(count==5)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t6_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==5)
           {
               n1=i;
           }
           if(count==6)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t7_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==6)
           {
               n1=i;
           }
           if(count==7)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t8_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==7)
           {
               n1=i;
           }
           if(count==8)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t9_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==8)
           {
               n1=i;
           }
           if(count==9)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t10_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==9)
           {
               n1=i;
           }
           if(count==10)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t11_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==10)
           {
               n1=i;
           }
           if(count==11)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t12_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==11)
           {
               n1=i;
           }
           if(count==12)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t13_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==12)
           {
               n1=i;
           }
           if(count==13)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t14_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==13)
           {
               n1=i;
           }
           if(count==14)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t15_1()
{
    QString qstr = ui->readed_value_t1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
              if(count==14)
              {
                  n1=i;
              }
              count++;
           }
        if ( ( qstr )[i] == ']')
           {
              if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u1_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
            count++;
        }
    }
    return qstr.mid(1,n1-1);
}

QString time_voltage::ui_read_from_file_u2_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
           if(count==2)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u3_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==2)
           {
               n1=i;
           }
           if(count==3)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u4_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==3)
           {
               n1=i;
           }
           if(count==4)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u5_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==4)
           {
               n1=i;
           }
           if(count==5)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u6_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==5)
           {
               n1=i;
           }
           if(count==6)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u7_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==6)
           {
               n1=i;
           }
           if(count==7)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u8_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==7)
           {
               n1=i;
           }
           if(count==8)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u9_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==8)
           {
               n1=i;
           }
           if(count==9)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u10_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==9)
           {
               n1=i;
           }
           if(count==10)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u11_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==10)
           {
               n1=i;
           }
           if(count==11)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u12_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==11)
           {
               n1=i;
           }
           if(count==12)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u13_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==12)
           {
               n1=i;
           }
           if(count==13)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u14_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==13)
           {
               n1=i;
           }
           if(count==14)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u15_1()
{
    QString qstr = ui->readed_value_U1->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

        if ( ( qstr )[i] == ',')
           {
              if(count==14)
              {
                  n1=i;
              }
              count++;
           }
        if ( ( qstr )[i] == ']')
           {
              if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t1_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
            count++;
        }
    }
    return qstr.mid(1,n1-1);
}

QString time_voltage::ui_read_from_file_t2_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
           if(count==2)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t3_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==2)
           {
               n1=i;
           }
           if(count==3)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t4_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==3)
           {
               n1=i;
           }
           if(count==4)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t5_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==4)
           {
               n1=i;
           }
           if(count==5)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t6_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==5)
           {
               n1=i;
           }
           if(count==6)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t7_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==6)
           {
               n1=i;
           }
           if(count==7)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t8_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==7)
           {
               n1=i;
           }
           if(count==8)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t9_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==8)
           {
               n1=i;
           }
           if(count==9)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t10_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==9)
           {
               n1=i;
           }
           if(count==10)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t11_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==10)
           {
               n1=i;
           }
           if(count==11)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t12_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==11)
           {
               n1=i;
           }
           if(count==12)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t13_2()
{
    QString qstr = ui->readed_value_t2->text();
    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==12)
           {
               n1=i;
           }
           if(count==13)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t14_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==13)
           {
               n1=i;
           }
           if(count==14)
           {
               n2=i;
           }
            count++;
        }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_t15_2()
{
    QString qstr = ui->readed_value_t2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {
        if ( ( qstr )[i] == ',')
           {
              if(count==14)
              {
                  n1=i;
              }
              count++;
           }
        if ( ( qstr )[i] == ']')
           {
              if(count==15)
              {
                  n2=i;
              }
               count++;
           }
    }
    return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u1_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
            count++;
        }
    }
    return qstr.mid(1,n1-1);
}

QString time_voltage::ui_read_from_file_u2_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==1)
           {
               n1=i;
           }
           if(count==2)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u3_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==2)
           {
               n1=i;
           }
           if(count==3)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u4_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==3)
           {
               n1=i;
           }
           if(count==4)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u5_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==4)
           {
               n1=i;
           }
           if(count==5)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u6_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==5)
           {
               n1=i;
           }
           if(count==6)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u7_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==6)
           {
               n1=i;
           }
           if(count==7)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u8_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==7)
           {
               n1=i;
           }
           if(count==8)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u9_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==8)
           {
               n1=i;
           }
           if(count==9)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u10_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==9)
           {
               n1=i;
           }
           if(count==10)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u11_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==10)
           {
               n1=i;
           }
           if(count==11)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u12_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==11)
           {
               n1=i;
           }
           if(count==12)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u13_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==12)
           {
               n1=i;
           }
           if(count==13)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u14_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==13)
           {
               n1=i;
           }
           if(count==14)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

QString time_voltage::ui_read_from_file_u15_2()
{
    QString qstr = ui->readed_value_U2->text();

    int n1 = 0;
    int n2 = 0;
    int count = 1;
    for ( int i = 0; i != qstr.size(); i++ )
    {

     if ( ( qstr )[i] == ',')
        {
           if(count==14)
           {
               n1=i;
           }
           count++;
        }
     if ( ( qstr )[i] == ']')
        {
           if(count==15)
           {
               n2=i;
           }
            count++;
        }
    }
     return qstr.mid(n1+1,n2-n1-1);
}

void time_voltage::on_rewriteButton_clicked()
{
    QDesktopServices::openUrl(QUrl("file:D:\\RF station/Tables/main generatetables_09.09.2021/generatetables.py", QUrl::TolerantMode));
}
